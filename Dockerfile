FROM node:16-alpine as dependencies
WORKDIR /my_project
COPY package.json yarn.lock ./
RUN yarn

FROM node:lts as builder
WORKDIR /my_project
ARG ENV_VARIABLES_AVAILABLE

COPY . .
COPY --from=dependencies /my_project/node_modules ./node_modules
RUN yarn build

FROM node:lts as runner
WORKDIR /my_project

COPY --from=builder /my_project/next.config.js ./
COPY --from=builder /my_project/public ./public
COPY --from=builder /my_project/.next ./.next
COPY --from=builder /my_project/node_modules ./node_modules
COPY --from=builder /my_project/package.json ./package.json

EXPOSE 3000
CMD [ "yarn", "start" ]