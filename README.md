
# NSFO

This project is a website developed for cool sheep AI project.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Scripts](#scripts)

## Installation

To get started with this project, follow these steps:

1. Clone the repository: `git clone https://gitlab.com/nsfo/website/frontend.git`
2. Install the dependencies: `npm install` or `yarn install`

## Usage

Once the installation is complete, you can run the project using the following command:

```shell
`npm run dev`
`yarn run dev`
