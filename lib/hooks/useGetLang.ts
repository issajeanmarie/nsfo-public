"use client";
import { useSearchParams } from "next/navigation";

export const useGetLang = () => {
  try {
    const search = useSearchParams();

    const language = search && search.get("lang");

    return language || "";
  } catch (error) {
    return "";
  }
};
