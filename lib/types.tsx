export type HeroTextProps = {
  icon: string;
  title: string;
  text: string;
};

export type MainArticleProp = {
  title: string;
  description: string;
  content: string;
  slug: string;
  image: string;
  label: string;
  code: string;
};

export type TextProps = {
  title: string;
  content: string;
  button: string;
  icon: string;
  link: string;
};

export type ServicetProps = {
  number: string;
  title: string;
  content: string;
};

export type TermsProps = {
  title: string;
  image: string;
  content: string;
};

export type ArticleProps = {
  id: number;
  slug: string;
  title: string;
  isChecked: boolean;
  description: string;
  content: string;
  date: string;
  image: string;
  label: string;
  href: string;
  link: string;
  code: string;
};

export type ProfileProps = {
  name: string;
  occupation: string;
  image: string;
};
