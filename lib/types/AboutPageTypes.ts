export interface AboutPageTypes {
  id: number;
  attributes: Attributes;
}
export interface Attributes {
  name: string;
  slug: string;
  Seo: Seo;
  Services?:
    | {
        id: number;
        Header: Header;
        service: Service;
        Navigation: Navigation;
      }[]
    | null;
  Terms: Terms;
  createdAt: string;
  updatedAt: string;
  publishedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  localizations: Localizations;
  locale: string;
}
export interface Seo {
  id: number;
  metaTitle: string;
  metaDescription: string;
  MetaImage: MetaImage;
  MetaTags?: MetaTagsEntity[] | null;
  preventIndexing: boolean;
  structuredData: string;
}
export interface MetaImage {
  id: number;
  alt: string;
  media: Media;
}
export interface Media {
  data: Data1;
}
export interface Data1 {
  id: number;
  attributes: Attributes1;
}
export interface Attributes1 {
  name: string;
  alternativeText: string;
  caption: string;
  width: number;
  height: number;
  formats: string;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  previewUrl: string;
  provider: string;
  provider_metadata: string;
  related: UsersOrRelatedOrChildren;
  folder: Folder;
  folderPath: string;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface UsersOrRelatedOrChildren {
  data?: DataEntityOrData[] | null;
}
export interface DataEntityOrData {
  id: number;
  attributes: AttributesOrMeta;
}
export interface AttributesOrMeta {}
export interface Folder {
  data: Data2;
}
export interface Data2 {
  id: number;
  attributes: Attributes2;
}
export interface Attributes2 {
  name: string;
  pathId: number;
  parent: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  children: UsersOrRelatedOrChildren;
  files: Files;
  path: string;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService {
  data: DataEntityOrData;
}
export interface Files {
  data?: DataEntity[] | null;
}
export interface DataEntity {
  id: number;
  attributes: Attributes3;
}
export interface Attributes3 {
  name: string;
  alternativeText: string;
  caption: string;
  width: number;
  height: number;
  formats: string;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  previewUrl: string;
  provider: string;
  provider_metadata: string;
  related: UsersOrRelatedOrChildren;
  folder: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  folderPath: string;
  createdAt: string;
  updatedAt: string;
  createdBy: CreatedBy;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface CreatedBy {
  data: Data3;
}
export interface Data3 {
  id: number;
  attributes: Attributes4;
}
export interface Attributes4 {
  firstname: string;
  lastname: string;
  username: string;
  email: string;
  resetPasswordToken: string;
  registrationToken: string;
  isActive: boolean;
  roles: Roles;
  blocked: boolean;
  preferedLanguage: string;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface Roles {
  data?: DataEntity1[] | null;
}
export interface DataEntity1 {
  id: number;
  attributes: Attributes5;
}
export interface Attributes5 {
  name: string;
  code: string;
  description: string;
  users: UsersOrRelatedOrChildren;
  permissions: Permissions;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface Permissions {
  data?: DataEntity2[] | null;
}
export interface DataEntity2 {
  id: number;
  attributes: Attributes6;
}
export interface Attributes6 {
  action: string;
  subject: string;
  properties: string;
  conditions: string;
  role: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface MetaTagsEntity {
  id: number;
  name: string;
  content: string;
}

export interface Header {
  id: number;
  label: string;
  title: string;
}
export interface Service {
  data: Data4;
}
export interface Data4 {
  id: number;
  attributes: Attributes7;
}
export interface Attributes7 {
  title: string;
  description: string;
  image: ImageOrIcon;
  categories: Categories;
  summary: string;
  createdAt: string;
  updatedAt: string;
  publishedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  localizations: Localizations;
  locale: string;
}
export interface ImageOrIcon {
  data: Data5;
}
export interface Data5 {
  id: number;
  attributes: Attributes8;
}
export interface Attributes8 {
  name: string;
  alternativeText: string;
  caption: string;
  width: number;
  height: number;
  formats: string;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  previewUrl: string;
  provider: string;
  provider_metadata: string;
  related: UsersOrRelatedOrChildren;
  folder: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  folderPath: string;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface Categories {
  data?: DataEntity3[] | null;
}
export interface DataEntity3 {
  id: number;
  attributes: Attributes9;
}
export interface Attributes9 {
  [x: string]: any;
  title: string;
  description: string;
  image: ImageOrIcon;
  service: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  createdAt: string;
  updatedAt: string;
  publishedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  localizations: Localizations;
  locale: string;
}
export interface Localizations {
  data?: string[] | null;
}
export interface Navigation {
  id: number;
  name: string;
  label: string;
  href: string;
  icon: ImageOrIcon;
}
export interface Terms {
  id: number;
  Header: Header;
  description: string;
  terms: Terms1;
}
export interface Terms1 {
  data?: DataEntity4[] | null;
}
export interface DataEntity4 {
  id: number;
  attributes: Attributes10;
}
export interface Attributes10 {
  [x: string]: any;
  title: string;
  description: string;
  icon: ImageOrIcon;
  createdAt: string;
  updatedAt: string;
  publishedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  localizations: Localizations;
  locale: string;
}
