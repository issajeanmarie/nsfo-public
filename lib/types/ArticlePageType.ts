
  export interface ArticlePageType {
    id: number;
    attributes: Attributes;
  }
  export interface Attributes {
    createdAt: string;
    updatedAt: string;
    publishedAt: string;
    locale: string;
    Header: Header;
    MoreArticles: MoreArticles;
    localizations: Localizations;
  }
  export interface Header {
    id: number;
    label: string;
    title: string;
  }
  export interface MoreArticles {
    id: number;
    perPage: number;
    title: string;
    Button: Button;
  }
  export interface Button {
    id: number;
    label: string;
    icon: Icon;
  }
  export interface Icon {
    data?: null;
  }
  export interface Localizations {
    data?: (DataEntity)[] | null;
  }
  export interface DataEntity {
    id: number;
    attributes: Attributes1;
  }
  export interface Attributes1 {
    createdAt: string;
    updatedAt: string;
    publishedAt: string;
    locale: string;
  }
  export interface Meta {
  }
  