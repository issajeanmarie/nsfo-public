export interface ArticlesType {
  [x: string]: any;
  id: number;
  attributes: Attributes;
}
export interface Attributes {
  [x: string]: any;
  title: string;
  slug: string;
  description: string;
  content: string;
  cover: Cover;
  categories: Categories;
  createdAt: string;
  updatedAt: string;
  publishedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  localizations: Localizations;
  locale: string;
}
export interface Cover {
  data: Data;
}
export interface Data {
  id: number;
  attributes: Attributes1;
}
export interface Attributes1 {
  name: string;
  alternativeText: string;
  caption: string;
  width: number;
  height: number;
  formats: string;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  previewUrl: string;
  provider: string;
  provider_metadata: string;
  related: UsersOrRelatedOrChildrenOrCategories;
  folder: Folder;
  folderPath: string;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
}
export interface UsersOrRelatedOrChildrenOrCategories {
  data?: DataEntityOrData[] | null;
}
export interface DataEntityOrData {
  id: number;
  attributes: Attributes2;
}
export interface Attributes2 {}
export interface Folder {
  data: Data1;
}
export interface Data1 {
  id: number;
  attributes: Attributes3;
}
export interface Attributes3 {
  name: string;
  pathId: number;
  parent: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  children: UsersOrRelatedOrChildrenOrCategories;
  files: Files;
  path: string;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
}
export interface RoleOrCreatedByOrUpdatedByOrFolderOrParent {
  data: DataEntityOrData;
}
export interface Files {
  data?: DataEntity1[] | null;
}
export interface DataEntity1 {
  id: number;
  attributes: Attributes4;
}
export interface Attributes4 {
  name: string;
  alternativeText: string;
  caption: string;
  width: number;
  height: number;
  formats: string;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  previewUrl: string;
  provider: string;
  provider_metadata: string;
  related: UsersOrRelatedOrChildrenOrCategories;
  folder: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  folderPath: string;
  createdAt: string;
  updatedAt: string;
  createdBy: CreatedBy;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
}
export interface CreatedBy {
  data: Data2;
}
export interface Data2 {
  id: number;
  attributes: Attributes5;
}
export interface Attributes5 {
  firstname: string;
  lastname: string;
  username: string;
  email: string;
  resetPasswordToken: string;
  registrationToken: string;
  isActive: boolean;
  roles: Roles;
  blocked: boolean;
  preferedLanguage: string;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
}
export interface Roles {
  data?: DataEntity2[] | null;
}
export interface DataEntity2 {
  id: number;
  attributes: Attributes6;
}
export interface Attributes6 {
  name: string;
  code: string;
  description: string;
  users: UsersOrRelatedOrChildrenOrCategories;
  permissions: Permissions;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
}
export interface Permissions {
  data?: DataEntity3[] | null;
}
export interface DataEntity3 {
  id: number;
  attributes: Attributes7;
}
export interface Attributes7 {
  action: string;
  subject: string;
  properties: string;
  conditions: string;
  role: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
}
export interface Categories {
  data?: DataEntity4[] | null;
}
export interface DataEntity4 {
  id: number;
  attributes: Attributes8;
}
export interface Attributes8 {
  title: string;
  description: string;
  articles: Articles;
  createdAt: string;
  updatedAt: string;
  publishedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  localizations: Localizations;
  locale: string;
}
export interface Articles {
  data?: DataEntity5[] | null;
}
export interface DataEntity5 {
  id: number;
  attributes: Attributes9;
}
export interface Attributes9 {
  title: string;
  slug: string;
  description: string;
  content: string;
  cover: Cover1;
  categories: UsersOrRelatedOrChildrenOrCategories;
  createdAt: string;
  updatedAt: string;
  publishedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  localizations: Localizations;
  locale: string;
}
export interface Cover1 {
  data: Data3;
}
export interface Data3 {
  id: number;
  attributes: Attributes10;
}
export interface Attributes10 {
  name: string;
  alternativeText: string;
  caption: string;
  width: number;
  height: number;
  formats: string;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  previewUrl: string;
  provider: string;
  provider_metadata: string;
  related: UsersOrRelatedOrChildrenOrCategories;
  folder: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  folderPath: string;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
}
export interface Localizations {
  data?: string[] | null;
}
export interface Meta {
  pagination: Pagination;
}
export interface Pagination {
  page: number;
  pageSize: number;
  pageCount: number;
  total: number;
}
