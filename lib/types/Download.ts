export interface Download {
    data?: (DataEntity)[] | null;
    meta: Meta;
  }
  export interface DataEntity {
    id: number;
    attributes: Attributes;
  }
  export interface Attributes {
    name: string;
    type: Type;
    service: Service;
    icon: ImageOrIconOrFile;
    link: string;
    file: ImageOrIconOrFile;
    createdAt: string;
    updatedAt: string;
    publishedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    localizations: Localizations;
    locale: string;
  }
  export interface Type {
    data: Data;
  }
  export interface Data {
    id: number;
    attributes: Attributes1;
  }
  export interface Attributes1 {
    title: string;
    description: string;
    createdAt: string;
    updatedAt: string;
    publishedAt: string;
    createdBy: CreatedBy;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    localizations: Localizations;
    locale: string;
  }
  export interface CreatedBy {
    data: Data1;
  }
  export interface Data1 {
    id: number;
    attributes: Attributes2;
  }
  export interface Attributes2 {
    firstname: string;
    lastname: string;
    username: string;
    email: string;
    resetPasswordToken: string;
    registrationToken: string;
    isActive: boolean;
    roles: Roles;
    blocked: boolean;
    preferedLanguage: string;
    createdAt: string;
    updatedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  }
  export interface Roles {
    data?: (DataEntity1)[] | null;
  }
  export interface DataEntity1 {
    id: number;
    attributes: Attributes3;
  }
  export interface Attributes3 {
    name: string;
    code: string;
    description: string;
    users: UsersOrRelatedOrChildren;
    permissions: Permissions;
    createdAt: string;
    updatedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  }
  export interface UsersOrRelatedOrChildren {
    data?: (DataEntityOrData)[] | null;
  }
  export interface DataEntityOrData {
    id: number;
    attributes: Attributes4;
  }
  export interface Attributes4 {
  }
  export interface Permissions {
    data?: (DataEntity2)[] | null;
  }
  export interface DataEntity2 {
    id: number;
    attributes: Attributes5;
  }
  export interface Attributes5 {
    action: string;
    subject: string;
    properties: string;
    conditions: string;
    role: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    createdAt: string;
    updatedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  }
  export interface RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService {
    data: DataEntityOrData;
  }
  export interface Localizations {
    data?: (string)[] | null;
  }
  export interface Service {
    data: Data2;
  }
  export interface Data2 {
    id: number;
    attributes: Attributes6;
  }
  export interface Attributes6 {
    title: string;
    description: string;
    image: Image;
    categories: Categories;
    summary: string;
    createdAt: string;
    updatedAt: string;
    publishedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    localizations: Localizations;
    locale: string;
  }
  export interface Image {
    data: Data3;
  }
  export interface Data3 {
    id: number;
    attributes: Attributes7;
  }
  export interface Attributes7 {
    name: string;
    alternativeText: string;
    caption: string;
    width: number;
    height: number;
    formats: string;
    hash: string;
    ext: string;
    mime: string;
    size: number;
    url: string;
    previewUrl: string;
    provider: string;
    provider_metadata: string;
    related: UsersOrRelatedOrChildren;
    folder: Folder;
    folderPath: string;
    createdAt: string;
    updatedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  }
  export interface Folder {
    data: Data4;
  }
  export interface Data4 {
    id: number;
    attributes: Attributes8;
  }
  export interface Attributes8 {
    name: string;
    pathId: number;
    parent: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    children: UsersOrRelatedOrChildren;
    files: Files;
    path: string;
    createdAt: string;
    updatedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  }
  export interface Files {
    data?: (DataEntityOrData1)[] | null;
  }
  export interface DataEntityOrData1 {
    id: number;
    attributes: Attributes9;
  }
  export interface Attributes9 {
    name: string;
    alternativeText: string;
    caption: string;
    width: number;
    height: number;
    formats: string;
    hash: string;
    ext: string;
    mime: string;
    size: number;
    url: string;
    previewUrl: string;
    provider: string;
    provider_metadata: string;
    related: UsersOrRelatedOrChildren;
    folder: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    folderPath: string;
    createdAt: string;
    updatedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  }
  export interface Categories {
    data?: (DataEntity3)[] | null;
  }
  export interface DataEntity3 {
    id: number;
    attributes: Attributes10;
  }
  export interface Attributes10 {
    title: string;
    description: string;
    image: ImageOrIconOrFile;
    service: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    createdAt: string;
    updatedAt: string;
    publishedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
    localizations: Localizations;
    locale: string;
  }
  export interface ImageOrIconOrFile {
    data: DataEntityOrData1;
  }
  export interface Meta {
    pagination: Pagination;
  }
  export interface Pagination {
    page: number;
    pageSize: number;
    pageCount: number;
    total: number;
  }
  