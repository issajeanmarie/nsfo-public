
export interface DownloadPageTypes {
  id: number;
  attributes: Attributes;
}
export interface Attributes {
  Header: Header;
  description: string;
  Downloads?: (DownloadsEntity)[] | null;
  createdAt: string;
  updatedAt: string;
  publishedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  localizations: Localizations;
  locale: string;
}
export interface Header {
  id: number;
  label: string;
  title: string;
}
export interface DownloadsEntity {
  id: number;
  __component: string;
  SortButton: SortButton;
  perPage: number;
  service: Service;
  Table: Table;
}
export interface SortButton {
  id: number;
  label: string;
  icon: Icon;
}
export interface Icon {
  data: Data1;
}
export interface Data1 {
  id: number;
  attributes: Attributes1;
}
export interface Attributes1 {
  name: string;
  alternativeText: string;
  caption: string;
  width: number;
  height: number;
  formats: string;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  previewUrl: string;
  provider: string;
  provider_metadata: string;
  related: UsersOrRelatedOrChildren;
  folder: Folder;
  folderPath: string;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface UsersOrRelatedOrChildren {
  data?: (DataEntityOrData)[] | null;
}
export interface DataEntityOrData {
  id: number;
  attributes: AttributesOrMeta;
}
export interface AttributesOrMeta {
}
export interface Folder {
  data: Data2;
}
export interface Data2 {
  id: number;
  attributes: Attributes2;
}
export interface Attributes2 {
  name: string;
  pathId: number;
  parent: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  children: UsersOrRelatedOrChildren;
  files: Files;
  path: string;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService {
  data: DataEntityOrData;
}
export interface Files {
  data?: (DataEntity)[] | null;
}
export interface DataEntity {
  id: number;
  attributes: Attributes3;
}
export interface Attributes3 {
  name: string;
  alternativeText: string;
  caption: string;
  width: number;
  height: number;
  formats: string;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  previewUrl: string;
  provider: string;
  provider_metadata: string;
  related: UsersOrRelatedOrChildren;
  folder: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  folderPath: string;
  createdAt: string;
  updatedAt: string;
  createdBy: CreatedBy;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface CreatedBy {
  data: Data3;
}
export interface Data3 {
  id: number;
  attributes: Attributes4;
}
export interface Attributes4 {
  firstname: string;
  lastname: string;
  username: string;
  email: string;
  resetPasswordToken: string;
  registrationToken: string;
  isActive: boolean;
  roles: Roles;
  blocked: boolean;
  preferedLanguage: string;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface Roles {
  data?: (DataEntity1)[] | null;
}
export interface DataEntity1 {
  id: number;
  attributes: Attributes5;
}
export interface Attributes5 {
  name: string;
  code: string;
  description: string;
  users: UsersOrRelatedOrChildren;
  permissions: Permissions;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface Permissions {
  data?: (DataEntity2)[] | null;
}
export interface DataEntity2 {
  id: number;
  attributes: Attributes6;
}
export interface Attributes6 {
  action: string;
  subject: string;
  properties: string;
  conditions: string;
  role: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface Service {
  data: Data4;
}
export interface Data4 {
  id: number;
  attributes: Attributes7;
}
export interface Attributes7 {
  title: string;
  description: string;
  image: ImageOrIcon;
  categories: Categories;
  summary: string;
  createdAt: string;
  updatedAt: string;
  publishedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  localizations: Localizations;
  locale: string;
}
export interface ImageOrIcon {
  data: Data5;
}
export interface Data5 {
  id: number;
  attributes: Attributes8;
}
export interface Attributes8 {
  name: string;
  alternativeText: string;
  caption: string;
  width: number;
  height: number;
  formats: string;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  previewUrl: string;
  provider: string;
  provider_metadata: string;
  related: UsersOrRelatedOrChildren;
  folder: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  folderPath: string;
  createdAt: string;
  updatedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface Categories {
  data?: (DataEntity3)[] | null;
}
export interface DataEntity3 {
  id: number;
  attributes: Attributes9;
}
export interface Attributes9 {
  title: string;
  description: string;
  image: ImageOrIcon;
  service: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  createdAt: string;
  updatedAt: string;
  publishedAt: string;
  createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
  localizations: Localizations;
  locale: string;
}
export interface Localizations {
  data?: (string)[] | null;
}
export interface Table {
  id: number;
  Columns?: (ColumnsEntity)[] | null;
  ActionButton: ActionButton;
}
export interface ColumnsEntity {
  id: number;
  name: string;
  title: string;
}
export interface ActionButton {
  id: number;
  label: string;
  icon: ImageOrIcon;
}
