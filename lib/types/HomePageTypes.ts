export interface HomePageResponseTypes {
	id: number;
	attributes: Attributes;
}
export interface Attributes {
	name: string;
	slug: string;
	Seo: Seo;
	Hero: Hero;
	Articles: Articles;
	Services: Services;
	Faqs: Faqs;
	Team: Team;
	createdAt: string;
	updatedAt: string;
	publishedAt: string;
	createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	localizations: Localizations;
	locale: string;
}
export interface Seo {
	id: number;
	metaTitle: string;
	metaDescription: string;
	MetaImage: MetaImage;
	MetaTags?: MetaTagsEntity[] | null;
	preventIndexing: boolean;
	structuredData: string;
}
export interface MetaImage {
	id: number;
	alt: string;
	media: Media;
}
export interface Media {
	data: Data1;
}
export interface Data1 {
	id: number;
	attributes: Attributes1;
}
export interface Attributes1 {
	name: string;
	alternativeText: string;
	caption: string;
	width: number;
	height: number;
	formats: string;
	hash: string;
	ext: string;
	mime: string;
	size: number;
	url: string;
	previewUrl: string;
	provider: string;
	provider_metadata: string;
	related: UsersOrRelatedOrChildrenOrArticles;
	folder: Folder;
	folderPath: string;
	createdAt: string;
	updatedAt: string;
	createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface UsersOrRelatedOrChildrenOrArticles {
	data?: DataEntityOrData[] | null;
}
export interface DataEntityOrData {
	id: number;
	attributes: {};
}
export interface Folder {
	data: Data2;
}
export interface Data2 {
	id: number;
	attributes: Attributes2;
}
export interface Attributes2 {
	name: string;
	pathId: number;
	parent: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	children: UsersOrRelatedOrChildrenOrArticles;
	files: Files;
	path: string;
	createdAt: string;
	updatedAt: string;
	createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService {
	data: DataEntityOrData;
}
export interface Files {
	data?: DataEntity[] | null;
}
export interface DataEntity {
	id: number;
	attributes: Attributes3;
}
export interface Attributes3 {
	name: string;
	alternativeText: string;
	caption: string;
	width: number;
	height: number;
	formats: string;
	hash: string;
	ext: string;
	mime: string;
	size: number;
	url: string;
	previewUrl: string;
	provider: string;
	provider_metadata: string;
	related: UsersOrRelatedOrChildrenOrArticles;
	folder: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	folderPath: string;
	createdAt: string;
	updatedAt: string;
	createdBy: CreatedBy;
	updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface CreatedBy {
	data: Data3;
}
export interface Data3 {
	id: number;
	attributes: Attributes4;
}
export interface Attributes4 {
	firstname: string;
	lastname: string;
	username: string;
	email: string;
	resetPasswordToken: string;
	registrationToken: string;
	isActive: boolean;
	roles: Roles;
	blocked: boolean;
	preferedLanguage: string;
	createdAt: string;
	updatedAt: string;
	createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface Roles {
	data?: DataEntity1[] | null;
}
export interface DataEntity1 {
	id: number;
	attributes: Attributes5;
}
export interface Attributes5 {
	name: string;
	code: string;
	description: string;
	users: UsersOrRelatedOrChildrenOrArticles;
	permissions: Permissions;
	createdAt: string;
	updatedAt: string;
	createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface Permissions {
	data?: DataEntity2[] | null;
}
export interface DataEntity2 {
	id: number;
	attributes: Attributes6;
}
export interface Attributes6 {
	action: string;
	subject: string;
	properties: string;
	conditions: string;
	role: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	createdAt: string;
	updatedAt: string;
	createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface MetaTagsEntity {
	id: number;
	name: string;
	content: string;
}
export interface Hero {
	id: number;
	text: string;
	backgroundMedia: ImageOrIconOrBackgroundMediaOrCoverOrAvatar;
	Services?: ServicesEntity[] | null;
}
export interface ImageOrIconOrBackgroundMediaOrCoverOrAvatar {
	data: Data4;
}
export interface Data4 {
	id: number;
	attributes: Attributes7;
}
export interface Attributes7 {
	name: string;
	alternativeText: string;
	caption: string;
	width: number;
	height: number;
	formats: string;
	hash: string;
	ext: string;
	mime: string;
	size: number;
	url: string;
	previewUrl: string;
	provider: string;
	provider_metadata: string;
	related: UsersOrRelatedOrChildrenOrArticles;
	folder: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	folderPath: string;
	createdAt: string;
	updatedAt: string;
	createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
}
export interface ServicesEntity {
	id: number;
	service: Service;
	icon: ImageOrIconOrBackgroundMediaOrCoverOrAvatar;
	Navigation: Navigation;
}
export interface Service {
	data: Data5;
}
export interface Data5 {
	id: number;
	attributes: Attributes8;
}
export interface Attributes8 {
	title: string;
	description: string;
	image: ImageOrIconOrBackgroundMediaOrCoverOrAvatar;
	categories: Categories;
	summary: string;
	createdAt: string;
	updatedAt: string;
	publishedAt: string;
	createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	localizations: Localizations;
	locale: string;
}
export interface Categories {
	data?: DataEntity3[] | null;
}
export interface DataEntity3 {
	id: number;
	attributes: Attributes9;
}
export interface Attributes9 {
	title: string;
	description: string;
	image: ImageOrIconOrBackgroundMediaOrCoverOrAvatar;
	service: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	createdAt: string;
	updatedAt: string;
	publishedAt: string;
	createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	localizations: Localizations;
	locale: string;
}
export interface Localizations {
	data?: string[] | null;
}
export interface Navigation {
	id: number;
	name: string;
	label: string;
	href: string;
	icon: ImageOrIconOrBackgroundMediaOrCoverOrAvatar;
}
export interface Articles {
	id: number;
	label: string;
	Articles?: ArticlesEntity[] | null;
}
export interface ArticlesEntity {
	id: number;
	article: Article;
	main: boolean;
	Navigation: Navigation;
}
export interface Article {
	data: Data6;
}
export interface Data6 {
	id: number;
	attributes: Attributes10;
}
export interface Attributes10 {
	title: string;
	slug: string;
	description: string;
	content: string;
	cover: ImageOrIconOrBackgroundMediaOrCoverOrAvatar;
	categories: Categories1;
	createdAt: string;
	updatedAt: string;
	publishedAt: string;
	createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	localizations: Localizations;
	locale: string;
}
export interface Categories1 {
	data?: DataEntity4[] | null;
}
export interface DataEntity4 {
	id: number;
	attributes: Attributes11;
}
export interface Attributes11 {
	title: string;
	description: string;
	articles: UsersOrRelatedOrChildrenOrArticles;
	createdAt: string;
	updatedAt: string;
	publishedAt: string;
	createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	localizations: Localizations;
	locale: string;
}
export interface Services {
	id: number;
	Header: Header;
	description: string;
	services: Services1;
}
export interface Header {
	id: number;
	label: string;
	title: string;
}
export interface Services1 {
	data?: DataEntity5[] | null;
}
export interface DataEntity5 {
	id: number;
	attributes: Attributes12;
}
export interface Attributes12 {
	label: string;
	title: string;
	description: string;
	createdAt: string;
	updatedAt: string;
	publishedAt: string;
	createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	localizations: Localizations;
	locale: string;
}
export interface Faqs {
	id: number;
	Header: Header;
	faqs: Faqs1;
}
export interface Faqs1 {
	data?: DataEntity6[] | null;
}
export interface DataEntity6 {
	id: number;
	attributes: Attributes13;
}
export interface Attributes13 {
	title: string;
	description: string;
	createdAt: string;
	updatedAt: string;
	publishedAt: string;
	createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	localizations: Localizations;
	locale: string;
}
export interface Team {
	id: number;
	Header: Header;
	description: string;
	team: Team1;
}
export interface Team1 {
	data?: DataEntity7[] | null;
}
export interface DataEntity7 {
	id: number;
	attributes: Attributes14;
}
export interface Attributes14 {
	name: string;
	title: string;
	avatar: ImageOrIconOrBackgroundMediaOrCoverOrAvatar;
	createdAt: string;
	updatedAt: string;
	publishedAt: string;
	createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParentOrService;
	localizations: Localizations;
	locale: string;
}
