export interface LayoutResponseTypes {
	id: number;
	attributes: Attributes;
}

export interface Attributes {
	Navbar: Navbar;
	Footer: Footer;
	createdAt: string;
	updatedAt: string;
	publishedAt: string;
	createdBy: { data: DataEntityOrData };
	updatedBy: { data: DataEntityOrData };
	localizations: Localizations;
	locale: string;
}

export interface Navbar {
	id: number;
	Navigations?: NavigationsEntity[] | null;
	Logo: LogoOrFooterImage;
	Locales?: LocalesEntity[] | null;
	Login: LoginOrEnroll;
	Enroll: LoginOrEnroll;
}

export interface NavigationsEntity {
	id: number;
	name: string;
	label: string;
	href: string;
	icon: Icon;
}
export interface Icon {
	data: Data1;
}
export interface Data1 {
	id: number;
	attributes: Attributes1;
}
export interface Attributes1 {
	name: string;
	alternativeText: string;
	caption: string;
	width: number;
	height: number;
	formats: string;
	hash: string;
	ext: string;
	mime: string;
	size: number;
	url: string;
	previewUrl: string;
	provider: string;
	provider_metadata: string;
	related: UsersOrRelatedOrChildren;
	folder: Folder;
	folderPath: string;
	createdAt: string;
	updatedAt: string;
	createdBy: { data: DataEntityOrData };
	updatedBy: { data: DataEntityOrData };
}
export interface UsersOrRelatedOrChildren {
	data?: DataEntityOrData[] | null;
}
export interface DataEntityOrData {
	id: number;
	attributes: {};
}

export interface Folder {
	data: Data2;
}
export interface Data2 {
	id: number;
	attributes: Attributes2;
}
export interface Attributes2 {
	name: string;
	pathId: number;
	parent: { data: DataEntityOrData };
	children: UsersOrRelatedOrChildren;
	files: Files;
	path: string;
	createdAt: string;
	updatedAt: string;
	createdBy: { data: DataEntityOrData };
	updatedBy: { data: DataEntityOrData };
}

export interface Files {
	data?: DataEntity[] | null;
}
export interface DataEntity {
	id: number;
	attributes: Attributes3;
}
export interface Attributes3 {
	name: string;
	alternativeText: string;
	caption: string;
	width: number;
	height: number;
	formats: string;
	hash: string;
	ext: string;
	mime: string;
	size: number;
	url: string;
	previewUrl: string;
	provider: string;
	provider_metadata: string;
	related: UsersOrRelatedOrChildren;
	folder: { data: DataEntityOrData };
	folderPath: string;
	createdAt: string;
	updatedAt: string;
	createdBy: CreatedBy;
	updatedBy: { data: DataEntityOrData };
}
export interface CreatedBy {
	data: Data3;
}
export interface Data3 {
	id: number;
	attributes: Attributes4;
}
export interface Attributes4 {
	firstname: string;
	lastname: string;
	username: string;
	email: string;
	resetPasswordToken: string;
	registrationToken: string;
	isActive: boolean;
	roles: Roles;
	blocked: boolean;
	preferedLanguage: string;
	createdAt: string;
	updatedAt: string;
	createdBy: { data: DataEntityOrData };
	updatedBy: { data: DataEntityOrData };
}
export interface Roles {
	data?: DataEntity1[] | null;
}
export interface DataEntity1 {
	id: number;
	attributes: Attributes5;
}
export interface Attributes5 {
	name: string;
	code: string;
	description: string;
	users: UsersOrRelatedOrChildren;
	permissions: Permissions;
	createdAt: string;
	updatedAt: string;
	createdBy: { data: DataEntityOrData };
	updatedBy: { data: DataEntityOrData };
}
export interface Permissions {
	data?: DataEntity2[] | null;
}
export interface DataEntity2 {
	id: number;
	attributes: Attributes6;
}
export interface Attributes6 {
	action: string;
	subject: string;
	properties: string;
	conditions: string;
	role: { data: DataEntityOrData };
	createdAt: string;
	updatedAt: string;
	createdBy: { data: DataEntityOrData };
	updatedBy: { data: DataEntityOrData };
}
export interface LogoOrFooterImage {
	id: number;
	alt: string;
	media: {
		data: {
			id: number;
			attributes: Attributes7;
		};
	};
}

export interface Attributes7 {
	name: string;
	alternativeText: string;
	caption: string;
	width: number;
	height: number;
	formats: string;
	hash: string;
	ext: string;
	mime: string;
	size: number;
	url: string;
	previewUrl: string;
	provider: string;
	provider_metadata: string;
	related: UsersOrRelatedOrChildren;
	folder: { data: DataEntityOrData };
	folderPath: string;
	createdAt: string;
	updatedAt: string;
	createdBy: { data: DataEntityOrData };
	updatedBy: { data: DataEntityOrData };
}
export interface LocalesEntity {
	id: number;
	name: string;
	label: string;
	code: string;
}
export interface LoginOrEnroll {
	id: number;
	name: string;
	label: string;
	href: string;
	icon: MediaOrIcon;
}
export interface Footer {
	id: number;
	Logo: LogoOrFooterImage;
	description: string;
	Contact: Contact;
	Newsletter: Newsletter;
	copyright: string;
	FooterImage: LogoOrFooterImage;
}
export interface Contact {
	id: number;
	title: string;
	Contacts?: ContactsEntity[] | null;
}
export interface ContactsEntity {
	id: number;
	name: string;
	label: string;
	icon: MediaOrIcon;
}
export interface Newsletter {
	id: number;
	title: string;
	description: string;
	Input: Input;
	Button: Button;
}

export interface Input {
	id: number;
	label: string;
	placeholder: string;
}

export interface Button {
	id: number;
	label: string;
	icon: MediaOrIcon;
}
export interface Localizations {
	data?: string[] | null;
}

export interface Weather {
	data: Data;
}

export interface Data {
	id: number;
	attributes: Attributes;
}

export interface Attributes {
	name: string;
	alternativeText: string;
	caption: string;
	width: number;
	height: number;
	formats: string;
	hash: string;
	ext: string;
	mime: string;
	size: number;
	url: string;
	previewUrl: string;
	provider: string;
	provider_metadata: string;
	related: Related;
	folder: FolderOrCreatedByOrUpdatedBy;
	folderPath: string;
	createdAt: string;
	updatedAt: string;
	createdBy: FolderOrCreatedByOrUpdatedBy;
	updatedBy: FolderOrCreatedByOrUpdatedBy;
}

export interface Related {
	data?: DataEntityOrData[] | null;
}

export interface DataEntityOrData {
	id: number;
	attributes: {};
}

export interface FolderOrCreatedByOrUpdatedBy {
	data: DataEntityOrData;
}

//HERE

export interface MediaOrIcon {
	data: MediaOrIconData;
}

export interface MediaOrIconData {
	id: number;
	attributes: MediaOrIconAttributes;
}

export interface MediaOrIconAttributes {
	name: string;
	alternativeText: string;
	caption: string;
	width: number;
	height: number;
	formats: string;
	hash: string;
	ext: string;
	mime: string;
	size: number;
	url: string;
	previewUrl: string;
	provider: string;
	provider_metadata: string;
	related: Related;
	folder: FolderOrCreatedByOrUpdatedBy;
	folderPath: string;
	createdAt: string;
	updatedAt: string;
	createdBy: FolderOrCreatedByOrUpdatedBy;
	updatedBy: FolderOrCreatedByOrUpdatedBy;
}

export interface Related {
	data?: DataEntityOrData[] | null;
}
export interface DataEntityOrData {
	id: number;
	attributes: {};
}
export interface FolderOrCreatedByOrUpdatedBy {
	data: DataEntityOrData;
}
