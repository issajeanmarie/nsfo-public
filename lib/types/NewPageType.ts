
  export interface NewsPage {
    id: number;
    attributes: Attributes;
  }
  export interface Attributes {
    name: string;
    slug: string;
    Seo: Seo;
    Header: Header;
    MainArticle: MainArticle;
    MoreArticles: MoreArticles;
    createdAt: string;
    updatedAt: string;
    publishedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    localizations: Localizations;
    locale: string;
  }
  export interface Seo {
    id: number;
    metaTitle: string;
    metaDescription: string;
    MetaImage: MetaImage;
    MetaTags?: (MetaTagsEntity)[] | null;
    preventIndexing: boolean;
    structuredData: string;
  }
  export interface MetaImage {
    id: number;
    alt: string;
    media: Media;
  }
  export interface Media {
    data: Data1;
  }
  export interface Data1 {
    id: number;
    attributes: Attributes1;
  }
  export interface Attributes1 {
    name: string;
    alternativeText: string;
    caption: string;
    width: number;
    height: number;
    formats: string;
    hash: string;
    ext: string;
    mime: string;
    size: number;
    url: string;
    previewUrl: string;
    provider: string;
    provider_metadata: string;
    related: UsersOrRelatedOrChildrenOrArticles;
    folder: Folder;
    folderPath: string;
    createdAt: string;
    updatedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  }
  export interface UsersOrRelatedOrChildrenOrArticles {
    data?: (DataEntityOrData)[] | null;
  }
  export interface DataEntityOrData {
    id: number;
    attributes: AttributesOrMeta;
  }
  export interface AttributesOrMeta {
  }
  export interface Folder {
    data: Data2;
  }
  export interface Data2 {
    id: number;
    attributes: Attributes2;
  }
  export interface Attributes2 {
    name: string;
    pathId: number;
    parent: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    children: UsersOrRelatedOrChildrenOrArticles;
    files: Files;
    path: string;
    createdAt: string;
    updatedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  }
  export interface RoleOrCreatedByOrUpdatedByOrFolderOrParent {
    data: DataEntityOrData;
  }
  export interface Files {
    data?: (DataEntity)[] | null;
  }
  export interface DataEntity {
    id: number;
    attributes: Attributes3;
  }
  export interface Attributes3 {
    name: string;
    alternativeText: string;
    caption: string;
    width: number;
    height: number;
    formats: string;
    hash: string;
    ext: string;
    mime: string;
    size: number;
    url: string;
    previewUrl: string;
    provider: string;
    provider_metadata: string;
    related: UsersOrRelatedOrChildrenOrArticles;
    folder: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    folderPath: string;
    createdAt: string;
    updatedAt: string;
    createdBy: CreatedBy;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  }
  export interface CreatedBy {
    data: Data3;
  }
  export interface Data3 {
    id: number;
    attributes: Attributes4;
  }
  export interface Attributes4 {
    firstname: string;
    lastname: string;
    username: string;
    email: string;
    resetPasswordToken: string;
    registrationToken: string;
    isActive: boolean;
    roles: Roles;
    blocked: boolean;
    preferedLanguage: string;
    createdAt: string;
    updatedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  }
  export interface Roles {
    data?: (DataEntity1)[] | null;
  }
  export interface DataEntity1 {
    id: number;
    attributes: Attributes5;
  }
  export interface Attributes5 {
    name: string;
    code: string;
    description: string;
    users: UsersOrRelatedOrChildrenOrArticles;
    permissions: Permissions;
    createdAt: string;
    updatedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  }
  export interface Permissions {
    data?: (DataEntity2)[] | null;
  }
  export interface DataEntity2 {
    id: number;
    attributes: Attributes6;
  }
  export interface Attributes6 {
    action: string;
    subject: string;
    properties: string;
    conditions: string;
    role: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    createdAt: string;
    updatedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  }
  export interface MetaTagsEntity {
    id: number;
    name: string;
    content: string;
  }
  export interface Header {
    id: number;
    label: string;
    title: string;
  }
  export interface MainArticle {
    id: number;
    article: Article;
    Button: Button;
  }
  export interface Article {
    data: Data4;
  }
  export interface Data4 {
    id: number;
    attributes: Attributes7;
  }
  export interface Attributes7 {
    title: string;
    slug: string;
    description: string;
    content: string;
    cover: CoverOrIcon;
    categories: Categories;
    createdAt: string;
    updatedAt: string;
    publishedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    localizations: Localizations;
    locale: string;
  }
  export interface CoverOrIcon {
    data: Data5;
  }
  export interface Data5 {
    id: number;
    attributes: Attributes8;
  }
  export interface Attributes8 {
    name: string;
    alternativeText: string;
    caption: string;
    width: number;
    height: number;
    formats: string;
    hash: string;
    ext: string;
    mime: string;
    size: number;
    url: string;
    previewUrl: string;
    provider: string;
    provider_metadata: string;
    related: UsersOrRelatedOrChildrenOrArticles;
    folder: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    folderPath: string;
    createdAt: string;
    updatedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
  }
  export interface Categories {
    data?: (DataEntity3)[] | null;
  }
  export interface DataEntity3 {
    id: number;
    attributes: Attributes9;
  }
  export interface Attributes9 {
    title: string;
    description: string;
    articles: UsersOrRelatedOrChildrenOrArticles;
    createdAt: string;
    updatedAt: string;
    publishedAt: string;
    createdBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    updatedBy: RoleOrCreatedByOrUpdatedByOrFolderOrParent;
    localizations: Localizations;
    locale: string;
  }
  export interface Localizations {
    data?: (string)[] | null;
  }
  export interface Button {
    id: number;
    label: string;
    icon: CoverOrIcon;
  }
  export interface MoreArticles {
    id: number;
    perPage: number;
    title: string;
    Button: Button;
  }
  