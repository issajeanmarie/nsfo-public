export interface APIResponseTypes<T> {
	data: T;
	meta: {};
}
