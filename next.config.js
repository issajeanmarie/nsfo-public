/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "oxawsbucket.s3.af-south-1.amazonaws.com",
      },
    ],
  },
};

module.exports = nextConfig;
