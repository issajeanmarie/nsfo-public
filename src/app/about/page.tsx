"use client";

import React from "react";
import AboutContent from "@/components/atoms/AboutContent";
import HeaderText from "@/components/HeaderText/HeaderText";
import Terms from "@/components/Services/Terms";
import { handleAPIRequests } from "../../../utils/handleAPIRequests";
import { APIResponseTypes } from "../../../lib/types/api";
import { AboutPageTypes } from "../../../lib/types/AboutPageTypes";
import Link from "next/link";
import { Switch } from "@/components/Switch/Switch";
import APIFallback from "@/components/atoms/APIFallback";
import { useGetLang } from "../../../lib/hooks/useGetLang";

async function About() {
  const language = useGetLang();
  const about_response: APIResponseTypes<AboutPageTypes> =
    await handleAPIRequests({ endpoint: "about-page", language });
  const services = about_response.data?.attributes?.Services;
  const terms = about_response.data?.attributes?.Terms;

  return (
    <APIFallback>
      <div className="flex flex-col ">
        {/* use state to get where it is showing the page, also add scroll to it */}
        <div className="bg-white z-10 fixed md:top-24 top-14 left-0 w-full  flex flex-col md:flex-row items-end justify-end px-5 md:px-16 ">
          <ul className="flex flex-col text-xs  md:space-y-0 md:space-x-12 space-x-0  space-y-2 md:flex-row  md:py-4 py-2 px-5  lg:px-[6rem] text-forest">
            {services?.map((link) => (
              <li key={link.id}>
                <Link
                  className={`text-xs text-right md:text-center hover:font-bold`}
                  // href={`about#${link.Header.title || ""}`}

                  href={`/about?lang=${language}#${link.Header.title}`}
                >
                  {link.Header.label} {link.Header.title}
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </div>
      <div
        className="flex flex-col bg-[#F5F5F5] pt-16  bg-no-repeat bg-right-top "
        style={{ backgroundSize: "21rem 21rem" }}
      >
        <div className=" flex flex-col relative p-5  md:px-16 lg:px-[6rem] xxl:max-w-[2100px]  xxl:mx-auto lg:py-[3rem] xl:px-[12rem] ">
          <div className="flex md:flex-row justify-between ">
            {services?.slice(0, 1).map((content) => (
              <div className="mt-24" key={content.id} id="health">
                <HeaderText
                  title={content.Header.label}
                  header={content.Header.title}
                  text={""}
                />
                <Switch
                  content={{
                    data: content.service.data,
                    link: content.Navigation,
                  }}
                />
              </div>
            ))}
          </div>
        </div>

        <div className="">
          {services?.slice(1).map((content, index: number) => (
            <div
              id={content?.Header?.title}
              key={content.id}
              className={` ${
                index % 2 === 0 ? "bg-cyan" : "bg-[#F5F5F5]"
              } flex-col flex `}
            >
              <div className="">
                <AboutContent
                  label={content.Header.label}
                  content={content?.Navigation?.label}
                  link={content?.Navigation?.href}
                  image={
                    content?.service?.data?.attributes?.image?.data?.attributes
                      ?.url
                  }
                  header={content.Header.title}
                  text={content.service.data?.attributes?.description}
                />
              </div>
            </div>
          ))}
        </div>

        <div className="w-full relative bg-cyan">
          <div
            className="md:block hidden bg-[url('/images/watermark.png')] bg-no-repeat bg-left-top w-full absolute bottom-0 h-full "
            style={{ backgroundSize: "21rem 21rem" }}
          ></div>
          <div className=" flex flex-col space-y-12 py-6 p-5  md:px-16 lg:px-[6rem]  lg:py-[3rem] xl:px-[12rem] md:py-12 xxl:max-w-[2100px]  xxl:mx-auto   ">
            <div className="md:w-3/5  w-full ">
              <div className="flex flex-row space-x-4">
                <div className="flex flex-col ">
                  <p className=" hidden md:block text-[8px] md:text-xs text-[#000000] font-semibold uppercase">
                    {terms?.Header?.label}
                  </p>
                  <div className="flex flex-row lg:space-x-4 mt-3">
                    <div className="hidden lg:block h-[45px] w-[4px] -ml-6 bg-green rounded-lg "></div>
                    <p className="font-black text-3xl  md:text-3xl lg:text-5xl leading-8  text-green md:w-4/6 ">
                      {terms?.Header.title}
                    </p>
                  </div>
                </div>
              </div>
              <p className="text-gray md:text-[14px] leading-6 mt-6">
                {terms?.description}
              </p>
            </div>
            <div className=" flex flex-col md:flex-row md:space-x-6">
              {terms?.terms?.data?.map((content) => (
                <Terms
                  key={content.attributes.title}
                  title={content.attributes.title}
                  image={content.attributes.icon.data.attributes.url}
                  content={content.attributes.description}
                />
              ))}
            </div>
          </div>
        </div>
      </div>
    </APIFallback>
  );
}

export default About;
