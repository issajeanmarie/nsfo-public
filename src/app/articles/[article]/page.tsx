"use client";
import Link from "next/link";
import Image from "next/image";
import React from "react";
import { APIResponseTypes } from "../../../../lib/types/api";
import { Attributes } from "../../../../lib/types/ArticlesType";
import { handleAPIRequests } from "../../../../utils/handleAPIRequests";
import moment from "moment";
import { useGetLang } from "../../../../lib/hooks/useGetLang";
import News from "@/components/News/News";
import { ArticlePageType } from "../../../../lib/types/ArticlePageType";
import ReactMarkdown from "react-markdown";

interface PageProps {
  params: { article: string };
}

const NewsId = async ({ params: { article: articleSlug } }: PageProps) => {
  const language = useGetLang();
  const article_page_response: APIResponseTypes<Attributes> =
    await handleAPIRequests({
      endpoint: "articles",
      language: language,
      filters: `filters[slug][$eq]=${articleSlug}`,
    });
  const articles_page_response: APIResponseTypes<Attributes> =
    await handleAPIRequests({
      endpoint: "articles",
      language: language,
    });
  const articles: APIResponseTypes<ArticlePageType> = await handleAPIRequests({
    endpoint: "article-page",
    language: language,
  });

  const mainArticle = article_page_response?.data[0];
  const moreArticles = articles_page_response.data;
  if (!mainArticle)
    return (
      <div className="flex-col flex items-center justify-center py-32 md:py-32  p-5 md:px-10 lg:px-[6rem]  xl:px-[12rem]  bg-[#F5F5F5] h-screen ">
        <p>Article Not found...</p>
      </div>
    );

  const formattedDate = moment(mainArticle.attributes.publishedAt).format("ll");

  return (
    <>
      <div
        className="flex-col flex py-16 md:py-32  p-5 md:px-10 lg:px-[6rem]  xl:px-[12rem]  bg-[#F5F5F5]  md:bg-[url('/images/watermark.png')] bg-no-repeat bg-right-top"
        style={{ backgroundSize: "21rem 21rem" }}
      >
        <div className="w-full md:w-2/3">
          <p className=" text-[12px] md:text-xs text-[#000000] font-semibold uppercase">
            <Link href="/articles">
              {articles.data.attributes.Header.title}&gt;
              {mainArticle?.attributes?.categories?.data?.length
                ? mainArticle?.attributes?.categories?.data[0]?.attributes
                    ?.title
                : ""}
            </Link>
          </p>

          <div className="flex flex-row md:mt-6 mb-6  lg:space-x-4 ">
            <div className="h-[2rem] bg-green mt-2 w-1 rounded-lg hidden lg:block md:-ml-6"></div>
            <div className="w-full">
              <p className="font-black text-3xl lg:text-4xl lg:leading-16 text-green uppercase  ">
                {mainArticle.attributes.title}
              </p>
            </div>
          </div>
        </div>

        <div className="flex flex-col space-y-6 md:space-y-0 md:flex-row md:mt-12">
          <div className=" flex flex-col  w-full lg:w-4/3  md:pr-12 lg:pr-24">
            <Image
              src={mainArticle.attributes.cover.data.attributes.url}
              alt=""
              width={992}
              height={588}
              className="w-full h-[40vh] lg:h-[60vh] xl:h-[45vh] object-cover "
            />
            <p className="text-xs italic text-[#919191] my-6">
              {formattedDate}
            </p>
            <ReactMarkdown>{mainArticle.attributes.content}</ReactMarkdown>
          </div>

          <div className="space-y-3 flex flex-col h-[40vh] md:h-screen w-full md:w-1/3 md:space-y-6 md:border-l-2 md:border-cyan ">
            <p className="text-base text-[#000000] underline font-bold border-t-2 py-2 md:py-0 md:border-0 border-cyan md:ml-12">
              {articles.data.attributes.MoreArticles.title}
            </p>
            <div className="flex items-center  justify-start h-screen ">
              <div className="md:ml-12 md:no-scrollbar flex  justify-start items-start md:flex-col md:space-x-0 space-x-6 md:space-y-12 overflow-hidden lg:w-2/3 w-full md:h-screen overflow-x-scroll md:overflow-y-scroll  py-2">
                {moreArticles?.map((article: any) => (
                  <News
                    key={article.id}
                    title={article.attributes.title}
                    date={article?.attributes.publishedAt}
                    isChecked={article.main}
                    id={article.id}
                    image={
                      article.attributes.cover.data.attributes.formats.small.url
                    }
                    description={article.attributes.description}
                    content={""}
                    slug={article.attributes.slug}
                    label={articles.data.attributes.MoreArticles.Button.label}
                    href={""}
                    link={""}
                    code={language}
                  />
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default NewsId;
