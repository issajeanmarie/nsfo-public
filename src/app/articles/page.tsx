"use client";
import APIFallback from "@/components/atoms/APIFallback";
import MainArticle from "@/components/atoms/MainArticle";
import HeaderText from "@/components/HeaderText/HeaderText";
import News from "@/components/News/News";
import React from "react";
import { APIResponseTypes } from "../../../lib/types/api";
import { Attributes } from "../../../lib/types/ArticlesType";
import { NewsPage } from "../../../lib/types/NewPageType";
import { handleAPIRequests } from "../../../utils/handleAPIRequests";

import { useGetLang } from "../../../lib/hooks/useGetLang";

const Article = async () => {
  const language = useGetLang();

  const article_page_response: APIResponseTypes<Attributes> =
    await handleAPIRequests({
      endpoint: "articles",
      language: language,
      sort: "publishedAt:desc",
    });

  const filtered_articles = article_page_response?.data;

  const main_article_response: APIResponseTypes<NewsPage> =
    await handleAPIRequests({
      endpoint: "news-page",
      language: language,
    });

  const mainArticle = main_article_response.data?.attributes?.MainArticle;
  const content = main_article_response.data?.attributes?.Header;

  return (
    <APIFallback>
      <div className="bg-[#f5f5f5]">
        <div
          className="pt-16 md:pt-32 lg:pt-36  pb-32 p-5  md:px-16 lg:px-[6rem]  lg:py-[3rem] xl:px-[12rem]  md:bg-[url('/images/watermark.png')] bg-no-repeat bg-right-top xxl:max-w-[2100px]  xxl:mx-auto "
          style={{ backgroundSize: "21rem 21rem" }}
        >
          <HeaderText title={content.label} header={content.title} text={""} />

          <div className="md:mt-12  ">
            <MainArticle
              title={mainArticle.article.data.attributes.title}
              description={mainArticle.article.data.attributes.description}
              image={
                mainArticle.article.data.attributes.cover.data.attributes.url
              }
              slug={mainArticle.article.data.attributes.slug}
              content={mainArticle.article.data.attributes.content}
              label={mainArticle.Button.label}
              code={language}
            />
          </div>

          <div className="w-full mt-12 py-6 md:py-32 grid grid-cols-2 gap-8 gap-y-[2.5rem] lg:grid-cols-4 lg:gap-x-12">
            {filtered_articles?.map((article: any) => (
              <News
                key={article?.id}
                title={article.attributes.title}
                date={article.attributes.publishedAt}
                isChecked={true}
                id={article?.id}
                description={article.attributes.description}
                image={article?.attributes?.cover?.data?.attributes?.url || ""}
                content={""}
                label={"Read more"}
                href={article?.slug || "#"}
                link={article?.attributes?.slug || "#"}
                slug={article?.attributes?.slug || "#"}
                code={language}
              />
            ))}
          </div>
        </div>
      </div>
    </APIFallback>
  );
};

export default Article;
