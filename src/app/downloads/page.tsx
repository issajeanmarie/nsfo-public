"use client";
import React from "react";
import { DownloadPageTypes } from "../../../lib/types/DownloadPageType";
import { DownloadType } from "../../../lib/types/DownloadPage";
import { APIResponseTypes } from "../../../lib/types/api";
import { handleAPIRequests } from "../../../utils/handleAPIRequests";
import Download from "@/components/Download/Download";
import APIFallback from "@/components/atoms/APIFallback";
import { useGetLang } from "../../../lib/hooks/useGetLang";

const Downloads = async () => {
  const language = useGetLang();
  const download_page_response: APIResponseTypes<DownloadPageTypes> =
    await handleAPIRequests({
      endpoint: "downloads-page",
      language,
    });

  const downloads_content_response: APIResponseTypes<DownloadType> =
    await handleAPIRequests({
      endpoint: "downloads",
      language,
      sort: "publishedAt:desc"
    });

  const downloadSection = download_page_response.data.attributes.Downloads;

  const downloadHeadings = download_page_response.data.attributes;

  return (
    <APIFallback>
      <div className="bg-[#F5F5F5]">
        {" "}
        <div
          className=" p-5  md:px-16 lg:px-[6rem] xl:px-[12rem] py-16 md:py-32  md:bg-[url('/images/watermark.png')] bg-no-repeat bg-right-top xxl:bg-center-top xxl:max-w-[2100px]  xxl:mx-auto"
          style={{ backgroundSize: "21rem 21rem" }}
        >
          <div className="flex flex-row space-x-4">
            <div className="flex flex-col ">
              <p className=" hidden md:block text-[8px] md:text-xs text-[#000000] font-semibold uppercase">
                {downloadHeadings.Header.label}
              </p>
              <div className="flex flex-row lg:space-x-4 mt-3">
                <div className="hidden lg:block h-[45px] w-[4px] -ml-6 mt-6 bg-green rounded-lg "></div>
                <p className="font-black text-3xl md:text-3xl lg:text-5xl  text-green w-12">
                  {downloadHeadings.Header.title}
                </p>
              </div>
              <p className="mt-3 md:mt-8 text-gray text-xs md:text-base  leading-7 font-normal md:w-3/4 w-full">
                {downloadHeadings.description}
              </p>
            </div>
          </div>
          <div className="mt-12">
            {downloadSection?.map((downloads) => (
              <React.Fragment key={downloads.id}>
                <Download
                  content={{
                    data: downloads_content_response,
                    table_section: downloads,
                  }}
                />
              </React.Fragment>
            ))}
          </div>
        </div>
      </div>
    </APIFallback>
  );
};

export default Downloads;
