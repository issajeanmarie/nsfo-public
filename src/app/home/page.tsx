"use client";
import Articles from "@/components/Articles/Articles";
import HeroCard from "@/components/Card/HeroCard";
import Profile from "@/components/Card/Profile";
import FrequentlyAskedQuestion from "@/components/FAQ/FrequentlyAskedQuestion";
import HeaderText from "@/components/HeaderText/HeaderText";
import Services from "@/components/Services/Services";
import APIFallback from "@/components/atoms/APIFallback";
import MainNews from "@/components/atoms/MainNews";
import { HomePageResponseTypes } from "../../../lib/types/HomePageTypes";
import { APIResponseTypes } from "../../../lib/types/api";
import { handleAPIRequests } from "../../../utils/handleAPIRequests";
import styles from "../styles/style.module.css";
import { useGetLang } from "../../../lib/hooks/useGetLang";
const Home = async () => {
  const language = useGetLang();

  const home_page_response: APIResponseTypes<HomePageResponseTypes> =
    await handleAPIRequests({
      endpoint: "home-page",
      language,
    });

  const hero = home_page_response?.data?.attributes?.Hero;
  const hero_cards = home_page_response?.data?.attributes?.Hero?.Services;
  const articles = home_page_response?.data?.attributes?.Articles?.Articles;
  const team_members = home_page_response?.data?.attributes?.Team?.team?.data;
  const services =
    home_page_response?.data?.attributes?.Services?.services?.data;
  const service_titles = home_page_response?.data?.attributes?.Services;
  const faq_titles = home_page_response?.data?.attributes?.Faqs;
  const team_titles = home_page_response?.data?.attributes?.Team;
  const faqs = home_page_response?.data?.attributes?.Faqs?.faqs;

  const main_article = articles?.find(
    (article: { main: boolean }) => article.main === true
  );

  const filtered_articles = articles?.filter(
    (article) => article.main === false
  );

  return (
    <APIFallback>
      <div className="bg-[#F5F5F5]">
        <div className="flex justify-center items-center w-full">
          <div className="w-full relative ">
            <div className="h-[80vh] md:h-[100vh] relative">
              <video
                autoPlay
                muted
                loop
                className="absolute top-0 left-0 w-full h-full object-cover"
              >
                <source
                  src={hero?.backgroundMedia?.data?.attributes?.url}
                  type={hero?.backgroundMedia?.data?.attributes?.mime}
                />
              </video>
            </div>
            <div className="absolute top-0 left-0 w-full h-full bg-[#000000] bg-opacity-75"></div>
            <div className="flex justify-center items-center h-full w-full absolute top-0 left-0 z-10">
              <p className="text-white font-semibold text-left text-[2rem]   px-12 md:px-[5rem] md:text-[3rem] lg:font-semibold md:text-center md:leading-[5rem]  lg:max-w-screen-md">
                {hero?.text}
              </p>
            </div>
          </div>
        </div>

        <div
          className="w-full flex flex-col  -mt-12 -py-16 md:-mt-40  md:pb-24 md:bg-[url('/images/watermark.png')] bg-no-repeat bg-right-top"
          style={{ backgroundSize: "21rem 21rem" }}
        >
          <div className="z-10 ">
            <div className="flex flex-col md:flex-row justify-center items-center md:items-start space-y-12 md:space-x-6 md:space-y-0 max-h-fit px-5 md:px-10 lg:px-[12rem] xl:px-[24rem] ">
              {hero_cards?.length &&
                hero_cards?.map((card) => (
                  <HeroCard
                    key={card.id}
                    icon={card?.icon?.data?.attributes?.url}
                    title={card?.service?.data?.attributes?.title}
                    content={card?.service?.data?.attributes?.summary}
                    button={card?.Navigation?.name}
                    link={card?.Navigation?.href}
                  />
                ))}
            </div>
          </div>

          <div
            className="p-5 md:px-10 lg:px-[6rem] lg:py-[3rem] xl:px-[12rem] md:bg-[url('/images/watermark.png')] bg-no-repeat bg-left xxl:max-w-[2100px]  xxl:mx-auto"
            style={{ backgroundSize: "21rem 21rem" }}
          >
            <MainNews
              label={home_page_response?.data?.attributes?.Articles?.label}
              title={main_article?.article?.data?.attributes?.title || ""}
              description={
                main_article?.article?.data?.attributes?.description || ""
              }
              image={
                main_article?.article?.data?.attributes?.cover?.data?.attributes
                  ?.url || ""
              }
              link={main_article?.Navigation?.label || ""}
              isChecked={false}
              date={""}
              content={""}
              href={main_article?.Navigation?.href || ""}
              id={0}
              slug={main_article?.article.data.attributes.slug || ""}
              code={language}
            />

            <div className=" w-full mt-6 md:mt-32 grid grid-cols-1 gap-[2rem] lg:gap-[4rem] lg:grid-cols-3 md:grid-cols-3  md:gap-7  ">
              {filtered_articles?.map((article) => (
                <Articles
                  key={article?.id}
                  title={article.article.data.attributes.title}
                  date={article.article.data.attributes.publishedAt}
                  isChecked={article.main}
                  id={article.article.data.id}
                  description={""}
                  image={
                    article?.article?.data?.attributes?.cover?.data?.attributes
                      ?.url || ""
                  }
                  content={""}
                  label={""}
                  href={article?.article?.data?.attributes?.slug || "#"}
                  link={article?.article?.data?.attributes?.slug || "#"}
                  slug={article?.article?.data?.attributes?.slug || "#"}
                  code={language}
                />
              ))}
            </div>
          </div>

          {/* service */}
          <div className="bg-white md:bg-[#f5f5f5]">
            <div
              className="lg:space-y-0 lg:space-x-12 flex flex-col lg:flex-row justify-between py-12  w-full p-5  md:px-10 lg:px-[6rem]  lg:py-[3rem] xl:px-[12rem] md:bg-[url('/images/watermark.png')] bg-no-repeat bg-left-bottom xxl:max-w-[2100px]  xxl:mx-auto"
              style={{ backgroundSize: "21rem 21rem" }}
            >
              <div className="w-full lg:w-1/3 justify-start items-start">
                <div className="flex flex-row space-x-4">
                  <div className="flex flex-col ">
                    <p className=" hidden md:block text-[8px] md:text-xs text-[#000000] font-semibold uppercase">
                      {service_titles?.Header?.label}
                    </p>
                    <div className="flex flex-row lg:space-x-4 mt-3">
                      {/* <div className="hidden md:block h-[45px] w-[8px] -ml-6  rounded-lg "></div> */}
                      <div className="hidden lg:block h-[45px] w-1 -ml-6 bg-green rounded-lg "></div>
                      <div className="w-full">
                        <p className="font-black text-3xl md:text-3xl lg:text-5xl  text-green">
                          {service_titles?.Header?.title}
                        </p>
                      </div>
                    </div>
                    <p className="mt-3 md:mt-8 text-gray text-xs md:text-base leading-6 font-normal md:w-3/4 w-full">
                      {service_titles?.description}
                    </p>
                  </div>
                </div>
              </div>

              <div className="w-full overflow-x-auto">
                <div className="flex flex-nowrap md:grid md:grid-cols-2 lg:grid-cols-3 md:space-x-0 gap-[1rem] md:gap-[2rem] lg:gap-x-[0.25rem]">
                  {services?.map((service) => (
                    <Services
                      key={service?.id}
                      number={service.attributes.label}
                      title={service.attributes.title}
                      content={service.attributes.description}
                    />
                  ))}
                </div>
              </div>
            </div>
          </div>

          {/* faq */}
          {/* xxl:ml-[25%]
          w-full-2100/2; ml */}
          <div
            className={`p-5  md:bg-[#f5f5f5] md:px-10 lg:px-[6rem] lg:py-[3rem] xl:px-[12rem]  lg:ml-0 xxl:max-w-[2100px] ${styles.container}`}
          >
            <HeaderText
              title={faq_titles?.Header?.label}
              header={faq_titles?.Header?.title}
              text=""
            />
            <div className="">
              <FrequentlyAskedQuestion faqs={faqs} />
            </div>
          </div>
        </div>

        <div className=" bg-white md:bg-[#91919126] opacity-0.5 ">
          <div className="flex flex-col space-y-3 p-5  md:px-10 lg:px-[6rem]  lg:py-[3rem] xl:px-[12rem]  xxl:max-w-[2100px]  xxl:mx-auto">
            <div className="flex flex-col-reverse md:flex-row ">
              <div className="hidden md:flex flex-nowrap md:flex-wrap lg:flex-wrap overflow-x-auto w-full md:w-2/3 space-x-2 ">
                {team_members?.slice(0, 2)?.map((member) => (
                  <Profile
                    key={member?.id}
                    image={member.attributes.avatar.data.attributes.url}
                    name={member.attributes.name}
                    occupation={member.attributes.title}
                  />
                ))}
              </div>

              <div className="w-full md:w-1/2  lg:pr-32 ">
                <HeaderText
                  title={team_titles?.Header?.label}
                  header={team_titles?.Header?.title}
                  text={team_titles?.description}
                />
              </div>
            </div>

            <div className="hidden md:flex flex-nowrap md:flex-wrap lg:flex-wrap overflow-x-auto w-full space-x-2 ">
              {team_members?.slice(2).map((member) => (
                <Profile
                  key={member?.id}
                  image={member?.attributes?.avatar?.data?.attributes?.url}
                  name={member?.attributes?.name}
                  occupation={member?.attributes?.title}
                />
              ))}
            </div>
            <div className="md:hidden flex flex-nowrap  overflow-x-auto w-full space-x-2 ">
              {team_members?.map((member) => (
                <Profile
                  key={member?.id}
                  image={member?.attributes?.avatar?.data?.attributes?.url}
                  name={member?.attributes?.name}
                  occupation={member?.attributes?.title}
                />
              ))}
            </div>
          </div>
        </div>
      </div>
    </APIFallback>
  );
};

export default Home;
