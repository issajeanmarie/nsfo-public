import Image from "next/image";
import React from "react";

function Landing() {
  return (
    <div className="flex justify-center items-center h-screen relative ">
      <div
        className="h-full w-full
       opacity-25 absolute"
      ></div>

      <div className=" z-20">
        <Image src="/images/nsfo.png" alt="NSFO Logo" className="w-72 h-28" width={300} height={200}  />
      </div>
    </div>
  );
}

export default Landing;
