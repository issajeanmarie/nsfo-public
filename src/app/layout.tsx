import "./globals.css";
import Layout from "@/components/Layout/Layout";

export default async function RootLayout(param: { children: React.ReactNode }) {
  // const search = useSearchParams();
  // const selected_lang = search.get("lang") || "";

  return (
    <html lang="en">
      <head>
        <link rel="icon" href="/images/icon.svg" />
      </head>
      <body>
        <>
          {/* @ts-expect-error Server Component */}
          <Layout>{param?.children || null}</Layout>
        </>
      </body>
    </html>
  );
}
