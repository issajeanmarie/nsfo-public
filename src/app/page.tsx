import React from "react";
import Home from "./home/page";

export default function Index() {
  return (
    <main className="flex flex-col">
      {/* @ts-expect-error Server Component */}
      <Home />
    </main>
  );
}
