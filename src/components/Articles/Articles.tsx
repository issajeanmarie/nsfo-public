import Image from "next/image";
import React from "react";
import { ArticleProps } from "../../../lib/types";
import moment from "moment";
import Link from "next/link";

const Articles: React.FC<ArticleProps> = ({
  title,
  date,
  image,
  slug,
  code,
}) => {
  const formattedDate = moment(date).format("ll");
  return (
    <>
      <div className="w-full space-y-4 md:border-0 border-b-1 pb-1 border-[#919191] border-b ">
        <Link href={`articles/${slug || ""}?lang=${code}`}>
          <div className="hidden md:block md:h-54 md:w-54 ">
            <Image
              src={image}
              alt=""
              className="h-[250px] w-full object-center object-cover "
              width={500}
              height={300}
            />
          </div>
          <div className="space-y-2 mt-2">
            <p className="text-xs md:text-xs italic text-[#919191]">
              {formattedDate}
            </p>
            <p className="text-[#656565] text-xs font-extrabold leading-7 md:text-base uppercase mt-2">
              {title}
            </p>
          </div>
        </Link>
      </div>
    </>
  );
};

export default Articles;
