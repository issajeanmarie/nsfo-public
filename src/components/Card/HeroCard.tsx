import Image from "next/image";
import Link from "next/link";
import React from "react";
import { TextProps } from "../../../lib/types";

const HeroCard: React.FC<TextProps> = ({
  title,
  content,
  button,
  icon,
  link,
}) => {
  return (
    <div className="w-full max-w-sm md:max-w-sm relative max-h-[30%]">
      <div className="hidden md:block h-16 md:h-40 w-full absolute top-0 left-0 border-solid border-x-2 border-t-2 border-white md:border-[f5f5f5] "></div>
      <div className="flex flex-col space-y-6 md:space-y-6 border-dotted border-2 border-green p-6 lg:py-16 lg:px-12  bg-white md:bg-[#f5f5f5]">
        {/*   */}
        <Image
          src={icon}
          className="h-8 w-8 md:h-6 md:w-6"
          alt=""
          width={100}
          height={200}
        />
        <p className="font-extrabold text-green md:uppercase  text-base lg:text-lg ">
          {title}
        </p>
        <p className="text-xs leading-5 font-[500] text-gray md:text-black  md:leading-6 grow-0 pr-5">
          {content}
        </p>

        <div className=" ">
          <button className="bg-gradient-to-r from-green to-forest w-full lg:w-4/5 py-3 text-white text-xs font-bold text-center uppercase">
            <Link target="_blank" href={link || ""}>
              {button}
            </Link>
          </button>
        </div>
      </div>
    </div>
  );
};

export default HeroCard;
