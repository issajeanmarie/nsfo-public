import Image from "next/image";
import React from "react";

import { ProfileProps } from "../../../lib/types";
const Profile: React.FC<ProfileProps> = ({ name, occupation, image }) => {
  return (
    <div className="">
      <div className="w-[180px] flex flex-col">
        <div className="">
          <Image
            src={image}
            alt=""
            width={200}
            height={200}
            className=" h-[213px] object-cover"
          />
        </div>

        <div className=" ml-4">
          <p className="text-xs font-semibold md:text-base  text-black ">
            {name}
          </p>
          <p className="font-normal text-xs text-gray ">{occupation}</p>
        </div>
      </div>
    </div>
  );
};

export default Profile;
