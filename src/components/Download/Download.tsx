"use client";
import React, { useState } from "react";
import Image from "next/image";
import _ from "lodash";
import { DownloadType } from "../../../lib/types/DownloadPage";
import { DownloadsEntity } from "../../../lib/types/DownloadPageType";
import Pagination from "../Pagination/Pagination";

interface DownloadProps {
  content: {
    data: any;
    table_section: DownloadsEntity;
  };
}

const paginate = (
  items: {
    image: string;
    name: string;
    type: string;
    link: string;
  }[],
  pageNumber: number,
  pageSize: number
) => {
  const startIndex = (pageNumber - 1) * pageSize;
  return _(items).slice(startIndex).take(pageSize).value();
};

const Download = ({ content }: DownloadProps) => {
  const filteredData = content.data.data;
  const table = content.table_section.Table;
  const section = content.table_section.service.data.id;
  const pageSize = 6;
  const [currentPage, setCurrentPage] = useState(1);
  const [sortBy, setSortBy] = useState(false);

  const handlePageChange = (page: number) => {
    setCurrentPage(page);
  };

  // const handleSortByButtonClick = () => {
  //   setSortBy(!sortBy);
  // };

  const category = filteredData?.filter(
    (category: DownloadType) =>
      category?.attributes?.service?.data?.id === section
  );

  const sortedCategory = _.orderBy(
    category,
    ["attributes.publishedAt"],
    [sortBy ? "desc" : "asc"]
  );
  const paginateData = paginate(sortedCategory, currentPage, pageSize);

  const handleDownload = (link: string) => {
    if (!link.endsWith(".pdf")) {
      const element = document.createElement("a");
      element.href = link;
      element.target = "_blank";
      element.download = link.substring(link.lastIndexOf("/") + 1);
      element.click();
    } else {
      window.open(link, "_blank");
    }
  };

  return (
    <>
      <div className="mx-auto">
        <div className="my-8 mb-32">
          <div className="w-full flex flex-row justify-between">
            <h2 className="text-[#919191] text-base md:text-[20px] font-bold">
              {content.table_section.service.data.attributes.title}
            </h2>
            {/* <div className="relative">
              <button
                onClick={handleSortByButtonClick}
                className="bg-white font-normal py-[0.26em] border-2 border-white w-full px-3 flex justify-center items-center space-x-2"
              >
                <p className="text-green text-center text-xs md:text-[13px]">
                  {content.table_section.SortButton.label}
                </p>
                <Image
                  src={
                    content.table_section.SortButton?.icon?.data?.attributes
                      ?.url || ""
                  }
                  height="12"
                  width="10"
                  alt={""}
                  className=""
                />
              </button>

              {sortBy && (
                <div className="absolute top-6 -right-4 mt-2 bg-white py-2 w-32 shadow">
                  <button
                    className="block w-full text-left py-4 px-2 mb-1 text-xs text-[#919191] hover:text-gray"
                    onClick={() => {
                      setSortBy(false);
                    }}
                  >
                    Newest to Oldest
                  </button>
                  <button
                    className="block w-full text-left py-1 px-2 text-xs text-[#919191] hover:text-gray"
                    onClick={() => {
                      // setSortBy(false);
                      setSortBy(true);
                    }}
                  >
                    Oldest to Newest
                  </button>
                </div>
              )}
            </div> */}

            <div className="relative">
              <select
                defaultValue=""
                onChange={(e) => setSortBy(e.target.value === "desc")}
                className="text-xs bg-white p-3 text-green focus:outline-none"
              >
                <option
                  disabled
                  value=" "
                  className=" focus:outline-none block w-full text-left py-4 px-2 mb-1 text-xs text-[#919191] hover:text-gray"
                >
                  {content.table_section.SortButton.label}
                </option>
                <option
                  className=" focus:outline-none block w-full text-left py-4 px-2 mb-1 text-xs text-[#919191] hover:text-graytext-[#919191] "
                  value="desc"
                >
                  Newest to Oldest
                </option>
                <option
                  value="asc"
                  className="focus:outline-none block w-full text-left py-4 px-2 mb-1 text-sm text-[#919191] hover:text-graytext-[#919191] "
                >
                  Oldest to Newest
                </option>
              </select>
            </div>
          </div>

          <table className="table-auto w-full mt-4">
            <thead className="w-full">
              <tr className="w-full">
                <th className="py-2 text-[#656565] text-xs md:text-[14px] w-4/8 text-start capitalize">
                  {table?.Columns?.[0].title}
                </th>
                <th className="text-[#656565] text-xs md:text-[14px] w-2/8 text-start hidden md:table-cell">
                  {table?.Columns?.[1].title}
                </th>
                <th className="text-[#656565] text-xs md:text-[14px] w-1/6 hidden md:table-cell text-end pr-12">
                  {table?.Columns?.[2].title}
                </th>
              </tr>
            </thead>
            {paginateData?.map((category: any) => (
              <tbody
                className="border-t-2 border-cyan w-full"
                key={category.id}
              >
                <tr className="border-b-2 border-cyan w-full">
                  <td className="border-none p-4 b flex flex-row space-x-2">
                    <Image
                      src={category?.attributes?.icon?.data?.attributes?.url}
                      alt="Article"
                      width={40}
                      height={40}
                      className="h-8 w-8 object-cover"
                    />
                    <div className="flex flex-col">
                      <p className="text-[#656565] text-xs italic md:text-[14px] md:mt-2">
                        {category?.attributes?.name}
                      </p>
                      <p className="text-[#656565] text-xs md:text-[14px] md:hidden mt-3 capitalize">
                        {category?.attributes?.type?.data?.attributes?.title}
                      </p>
                    </div>
                  </td>
                  <td className="border-none hidden md:table-cell">
                    <p className="text-xs md:text-[14px] text-[#656565] capitalize">
                      {category?.attributes?.type?.data?.attributes?.title}{" "}
                    </p>
                  </td>
                  <td className="border-none w-1/6 text-end pr-10">
                    <button
                      onClick={() =>
                        handleDownload(category?.attributes?.link || "")
                      }
                      className="px-2 py-3 bg-gradient-to-r from-green to-forest text-white md:text-green md:bg-gradient-to-r md:from-[#f5f5f5] md:to-[#f5f5f5] italic font-semibold text-xs md:text-[14px] md:underline"
                    >
                      {table?.ActionButton?.label}
                    </button>
                  </td>
                </tr>
              </tbody>
            ))}
          </table>

          {category.length > 0 && (
            <Pagination
              items={category.length}
              pageSize={pageSize}
              currentPage={currentPage}
              onPageChange={handlePageChange}
            />
          )}
        </div>
      </div>
    </>
  );
};

export default Download;
