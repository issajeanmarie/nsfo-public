"use client";

import Image from "next/image";
import { useState } from "react";
import { Attributes13, Faqs1 } from "../../../lib/types/HomePageTypes";

const FrequentlyAskedQuestion = ({ faqs }: { faqs: Faqs1 }) => {
  const frequent = faqs?.data;
  const [openedIndex, setOpenedIndex] = useState<number | null>(null);
  const [selectedItem, setSelectedItem] = useState<Attributes13>();

  const handleItemClick = (index: number) => {
    frequent && setSelectedItem(frequent[index]?.attributes || null);
    setOpenedIndex(index);
  };

  return (
    <div className=" flex flex-col w-full lg:flex-row md:justify-between items-start lg:space-x-3  lg:space-y-0 space-y-6 ">
      <div className="lg:w-7/12 w-full space-y-3  md:space-y-7 flex flex-col md:h-80">
        {frequent?.map((item, index) => (
          <div
            className="border-2 border-green text-gray text-xs px-8 py-1 w-full flex flex-row justify-between items-center  cursor-pointer"
            key={item?.id}
            onClick={() => handleItemClick(index)}
          >
            <div className="flex flex-col space-y-6 w-full">
              <div className=" ">
                <p className="text-[#000000] text-xs md:text-xs leading-5 md:leading-8  font-medium">
                  {item?.attributes?.title}
                </p>
              </div>
            </div>
            <Image
              src="/images/faq.svg"
              alt=""
              width={100}
              height={200}
              className={
                openedIndex === index
                  ? "md:rotate-0 rotate-90 h-4 w-4  "
                  : " md:rotate-90 rotate-0 h-4 w-4 "
              }
            />
          </div>
        ))}
      </div>

      {selectedItem && (
        <div className="border-2 border-gray bg-white lg:w-4/12 w-full flex flex-col space-y-6  lg:h-full lg:min-h-fit p-10">
          <p className="font-bold md:text-[21px] ">{selectedItem?.title}</p>
          <p className="text-gray text-xs md:text-[13px] leading-5  ">
            {selectedItem?.description}
          </p>
        </div>
      )}
    </div>
  );
};

export default FrequentlyAskedQuestion;
