import Image from "next/image";
import React from "react";
import { LayoutResponseTypes } from "../../../lib/types/LayoutTypes";
import { APIResponseTypes } from "../../../lib/types/api";
import SubscribeForm from "./SubscribeForm";

const Footer = ({
  layout_response,
}: {
  layout_response: APIResponseTypes<LayoutResponseTypes>;
}) => {
  const footer_descriptions = layout_response?.data?.attributes?.Footer;
  const newsletter = layout_response?.data?.attributes?.Footer?.Newsletter;
  const contact = layout_response?.data?.attributes?.Footer?.Contact;
  const image = layout_response?.data?.attributes?.Footer?.FooterImage;
  return (
    <div className="flex flex-col space-y-12 border-dashed border-green border-t-[1px] bg-[#F0F0F0]">
      <div className="flex flex-col md:flex-row justify-start items-start w-full py-14 px-5 lg:px-[6rem] lg:space-x-40 md:space-x-12 md:space-y-0 space-y-16 xxl:max-w-[2100px]  xxl:mx-auto ">
        <div className="flex flex-col  md:w-1/3 w-full  text-left space-y-4 md:space-y-14 ">
          <Image
            src={footer_descriptions?.Logo?.media?.data?.attributes?.url || ""}
            className="w-48"
            width={300}
            height={100}
            alt=""
          />
          <p className="text-gray text-xs leading-6 lg:pr-6">
            {footer_descriptions?.description}
          </p>
        </div>

        <div className="flex flex-col  md:w-1/3 w-full text-left space-y-4 md:space-y-14 justify-start items-start">
          <p className="text-green text-xl font-bold">{contact?.title}</p>
          <div className="content">
            <ul className="text-gray text-xs leading-8">
              {contact?.Contacts?.length &&
                contact?.Contacts.map(
                  (element, index: React.Key | null | undefined) => (
                    <li
                      className="flex flex-row space-x-2 items-center "
                      key={index}
                    >
                      <Image
                        src={element?.icon?.data?.attributes?.url || ""}
                        alt="Address icon"
                        width={300}
                        height={100}
                        className="w-[15px] h-[15px]"
                      />
                      <span>{element?.label}</span>
                    </li>
                  )
                )}
            </ul>
          </div>
        </div>

        <div className="flex flex-col  md:w-1/3 w-full  text-left space-y-6 md:space-y-12 ">
          <p className="text-green text-xl font-bold">{newsletter?.title}</p>
          <p className="text-gray text-xs leading-6 lg:pr-6">
            {newsletter?.description}
          </p>

          <SubscribeForm footer_descriptions={footer_descriptions} />
        </div>
      </div>

      <div className="w-full h-[30vh] relative">
        <Image
          src={image?.media?.data?.attributes?.url}
          className=" object-cover w-full h-full"
          width={300}
          height={100}
          alt=""
        />

        <p className=" z-5  font-bold text-xs md:text-sm text-white -mt-6 text-center mx-auto md:mb-6 lg:mb-12">
          {footer_descriptions?.copyright}
        </p>
      </div>
    </div>
  );
};

export default Footer;
