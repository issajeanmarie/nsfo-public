"use client";

import Image from "next/image";
import { ChangeEvent, FormEvent, useEffect, useState } from "react";
import MailchimpSubscribe from "react-mailchimp-subscribe";
import { Footer } from "../../../lib/types/LayoutTypes";

const urlMailChip =
  "https://nsfo.us12.list-manage.com/subscribe/post?u=73c6f9e7c883cb1b467d4796f&id=55ee701a41&f_id=00165ae0f0";

const SubscribeForm = ({
  footer_descriptions,
}: {
  footer_descriptions: Footer;
}) => {
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");

  return (
    <div className="w-full">
      <MailchimpSubscribe
        url={urlMailChip}
        render={({ subscribe, status, message }) => {
          return (
            <CustomForm
              subscribe={subscribe}
              footer_descriptions={footer_descriptions}
              status={status}
              message={message}
              setSuccess={setSuccess}
              setError={setError}
            />
          );
        }}
      />
      <p className="text-xs mt-2 text-[#f50747]">{error}</p>
      <p className="text-xs mt-2 text-[#459D68]">{success}</p>
    </div>
  );
};

export default SubscribeForm;

export const CustomForm = ({
  subscribe,
  footer_descriptions,
  status,
  message,
  setSuccess,
  setError,
}: {
  footer_descriptions: Footer;
  subscribe: any;
  // subscribe: ( arg0: EmailFormFields) => void;
  status: "error" | "success" | "sending" | null;
  message: string | Error | null;
  setSuccess: React.Dispatch<React.SetStateAction<string>>;
  setError: React.Dispatch<React.SetStateAction<string>>;
}) => {
  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState(false);

  const handleEmailChangeValue = (e: ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();

    setEmail(e?.target?.value);
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (email && email.indexOf("@") > -1) {
      subscribe({ EMAIL: email });
      setLoading(true);
    }
  };

  useEffect(() => {
    if (status === "error") {
      setLoading(false);
      setError(typeof message === "string" ? message : "Something went wrong");
      setSuccess("");
    } else if (status === "success") {
      setLoading(false);
      setError("");
      setSuccess(
        typeof message === "string" ? message : "Thanks for subscribing!"
      );
    } else {
      setError("");
      setSuccess("");
    }
  }, [status]);

  return (
    <form
      onSubmit={(e) => handleSubmit(e)}
      name="subscribe"
      className="flex w-full"
    >
      <input
        value={email}
        onChange={(e) => handleEmailChangeValue(e)}
        type="email"
        placeholder={footer_descriptions?.Newsletter?.Input?.placeholder}
        className="p-2 text-gray text-xs w-4/5 flex-grow-1 border-none focus:outline-none  "
      />
      <button className="bg-green text-white p-2" type="submit">
        <Image
          src={
            loading
              ? "./images/progress.svg"
              : footer_descriptions?.Newsletter?.Button?.icon?.data?.attributes
                  ?.url || "/images/send.svg"
          }
          alt=""
          className={`w-4 h-4 ${loading && "animate-spin"}`}
          width={300}
          height={100}
        />
      </button>
    </form>
  );
};
