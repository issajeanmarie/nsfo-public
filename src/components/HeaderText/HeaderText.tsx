import React from "react";

type HeaderTextProps = {
  title: string;
  header: string;
  text: string;
};

const HeaderText: React.FC<HeaderTextProps> = ({ title, header, text }) => {
  return (
    <div className="flex flex-row space-x-4">
      <div className="flex flex-col ">
        <p className=" hidden md:block text-[8px] md:text-xs text-[#000000] font-semibold uppercase">
          {title}
        </p>
        <div className="flex flex-row lg:space-x-4 mt-3">
          <div className="hidden lg:block h-[45px] w-[4px] -ml-6 bg-green rounded-lg "></div>
          <p className="font-black text-3xl md:text-3xl lg:text-5xl  text-green">
            {header}
          </p>
        </div>
        <p className="mt-3 md:mt-8 text-gray text-xs md:text-base leading-6 font-normal md:w-3/4 w-full">
          {text}
        </p>
      </div>
    </div>
  );
};

export default HeaderText;
