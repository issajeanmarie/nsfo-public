"use client";

import React from "react";
import { LayoutResponseTypes } from "../../../lib/types/LayoutTypes";
import { APIResponseTypes } from "../../../lib/types/api";
import { handleAPIRequests } from "../../../utils/handleAPIRequests";
import Footer from "../Footer/Footer";
import NavBar from "../Navbar/Navbar";
import { useGetLang } from "../../../lib/hooks/useGetLang";
// import { useGetLang } from "../../../lib/hooks/useGetLang";
// import {
//   LocalizationContext,
//   LocalizationProvider,
// } from "@/Context/Localization";

const Layout = async (param: { children: React.ReactNode }) => {
  const language = useGetLang();

  const layout_response: APIResponseTypes<LayoutResponseTypes> =
    await handleAPIRequests({ endpoint: "layout", language });

  return (
    <>
      {/* <LocalizationProvider> */}

      <NavBar layout_response={layout_response} />
      {param?.children || null}
      <Footer layout_response={layout_response} />
      {/* </LocalizationProvider> */}
    </>
  );
};

export default Layout;
