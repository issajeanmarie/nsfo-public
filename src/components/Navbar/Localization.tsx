"use client";

import Image from "next/image";
import { usePathname, useRouter } from "next/navigation";
import React, { useEffect, useState } from "react";
import { LocalesEntity } from "../../../lib/types/LayoutTypes";

const Localization = ({ data }: { data: LocalesEntity[] }) => {
  const [showLanguagePopup, setShowLanguagePopup] = useState(false);
  const [selectedLanguage, setSelectedLanguage] = useState(
    data.length ? data[0].code : "en"
  );
  const [langCode, setLangCode] = useState("nl");

  const router = useRouter();
  const pathname = usePathname();

  // Add a state to track the current viewport width
  const [viewportWidth, setViewportWidth] = useState(0);

  useEffect(() => {
    const handleResize = () => {
      setViewportWidth(window.innerWidth);
    };
    window.addEventListener("resize", handleResize);
    setViewportWidth(window.innerWidth);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const handleLanguageButtonClick = () => {
    setShowLanguagePopup(!showLanguagePopup);
  };

  const handleSelectLang = (code: string) => {
    setSelectedLanguage(code);
    console.log("clicked");
    router.push(pathname + "?" + "lang=" + code);
  };

  const alternateSelectedLanguage = (code: string) => {
    const newCode = code === "nl" ? "en" : "nl";
    setLangCode(newCode);
    console.log(`new code ${newCode}`);
    router.push(pathname + "?" + "lang=" + newCode);
  };

  // Check if the viewport width is less than or equal to the mobile breakpoint
  const isMobile = viewportWidth <= 768;

  return (
    <div className="flex flex-col lg:flex-row items-center justify-center  ">
      {!isMobile && (
        <button
          className="hidden md:flex flex-row justify-center items-center space-x-2 font-normal text-[12px]"
          onClick={handleLanguageButtonClick}
        >
          <p className="text-green ">{selectedLanguage}</p>
          <Image src="/images/down-nav.svg" alt="" width={8} height={8} />
        </button>
      )}

      {isMobile && (
        <button
          className="md:hidden flex flex-row justify-center items-center space-x-2 font-normal text-[12px]"
          onClick={() => {
            alternateSelectedLanguage(langCode);
            setShowLanguagePopup(false);
          }}
        >
          <p className="text-white text-base uppercase font-bold">{langCode}</p>
          <Image src="/images/down-nav.svg" alt="" width={8} height={8} />
        </button>
      )}

      {showLanguagePopup && (
        <div className="absolute lg:top-14 lg:right-10 xl:right-40 mt-2 bg-white p-6 shadow">
          <p className="text-[14px] mb-3 text-gray">Select Language</p>
          {data?.map((lang) => (
            <button
              key={lang?.id}
              className="block w-full text-left py-1 px-2 text-[12px]"
              onClick={() => {
                handleSelectLang(lang?.code);
                setShowLanguagePopup(false);
              }}
            >
              {lang.label}
            </button>
          ))}
        </div>
      )}
    </div>
  );
};

export default Localization;
