"use client";

import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";
import { AiOutlineClose, AiOutlineMenu } from "react-icons/ai";
import { LayoutResponseTypes } from "../../../lib/types/LayoutTypes";
import { APIResponseTypes } from "../../../lib/types/api";
import Localization from "./Localization";
import { useGetLang } from "../../../lib/hooks/useGetLang";
// import { useGetLang } from "../../../lib/hooks/useGetLang";

const NavBar = ({
  layout_response,
}: {
  layout_response: APIResponseTypes<LayoutResponseTypes>;
}) => {
  const [activeLink, setActiveLink] = useState("");
  const [isNavOpen, setIsNavOpen] = useState(false);
  const [isUserAtHome, setIsUserAtHome] = useState(false);
  const [isDotted, setIsDotted] = useState(false);
  const [isHomeNavbarVisible, setIsHomeNavbarVisible] = useState(false);

  const navigation = layout_response?.data?.attributes?.Navbar?.Navigations;
  const buttons = layout_response?.data?.attributes?.Navbar;
  const locales = layout_response?.data?.attributes?.Navbar.Locales || [];
  const pathname = usePathname();

  const code = useGetLang();

  const handleMobileNav = () => setIsNavOpen(!isNavOpen);

  const handleLinkClick = () => {
    if (isNavOpen) {
      setIsNavOpen(false);
    }
  };

  useEffect(() => {
    const handleScroll = () => {
      const scrollPosition = window.scrollY;
      setIsHomeNavbarVisible(scrollPosition >= 180);
    };

    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    if (pathname === "/") {
      setActiveLink(navigation?.length ? navigation[0]?.href : "");
      setIsUserAtHome(true);
      return;
    }

    const active_link = navigation?.find(
      (nav) => pathname.split("/").indexOf(nav.href) !== -1
    );

    setActiveLink(active_link?.href || "");
    setIsUserAtHome(false);

    if (pathname === "/about") {
      setIsDotted(true);
    } else {
      setIsDotted(false);
    }
  }, [pathname, navigation]);

  const is_menu_transparent = isUserAtHome && !isHomeNavbarVisible;

  return (
    <div
      style={{
        backgroundColor: is_menu_transparent ? "transparent" : "#fff",
      }}
      className={`z-30 fixed left-0 top-0  w-full h-14 xxl:h-28 md:h-24 px-5 md:px-16 xl:px-[12rem]  flex items-center justify-between border-b ${
        isDotted && !isUserAtHome
          ? "border-green border-dotted border-b-2"
          : "border-none"
      }`}
    >
      <Link href="/home">
        <Image
          src="/images/nsfo-logo.png"
          width={400}
          height={100}
          className="h-6 w-24"
          alt=""
        />
      </Link>

      <div className="links hidden lg:flex lg:flex-row lg:space-y-12 ml-52 ">
        <ul className="flex flex-row space-x-8 ">
          {navigation?.map((nav) => {
            return (
              <div key={nav?.id} className="">
                <li
                  style={{ color: is_menu_transparent ? "#FFFFFF" : "#00461C" }}
                  className={`text-xs xxl:text-sm text-center uppercase ${
                    activeLink === nav?.href ? "font-semibold" : "font-normal"
                  } hover:font-bold`}
                >
                  <Link
                    href={`${nav?.href}?lang=${code}`}
                    onClick={() => {
                      setActiveLink(nav?.href || "");
                      handleLinkClick(); // Call handleLinkClick to close the mobile menu
                    }}
                  >
                    {nav?.label}
                  </Link>
                </li>
                {activeLink === nav.href && (
                  <div className="w-full h-1 rounded-sm mt-2 bg-green"></div>
                )}
              </div>
            );
          })}
        </ul>
      </div>

      <div className="hidden lg:flex lg:flex-row lg:space-x-20">
        <div className="space-x-4">
          <button className="bg-gradient-to-r from-green to-forest text-white uppercase font-semibold py-2  px-11 rounded-none text-center text-xs xxl:text-sm">
            <Link href={buttons?.Login?.href || ""} target="_blank">
              {buttons?.Login?.label}
            </Link>
          </button>

          <button
            style={{ color: is_menu_transparent ? "#ffffff" : "#00461C" }}
            className="transparent  uppercase  border-2 border-green font-medium py-2 px-11 rounded-none text-center text-xs xxl:text-sm"
          >
            <Link href={buttons?.Enroll?.href || ""} target="_blank">
              {buttons?.Enroll?.label}
            </Link>
          </button>
        </div>

        <Localization data={locales} />
      </div>

      {/* Mobile nav open */}
      <div onClick={handleMobileNav} className="block lg:hidden z-10 ">
        {isNavOpen ? (
          <AiOutlineClose size={24} style={{ color: `#fff` }} />
        ) : (
          <AiOutlineMenu size={24} style={{ color: `#459D68` }} />
        )}
      </div>

      {/* Mobile menu */}
      <div
        onClick={handleMobileNav}
        className={
          isNavOpen
            ? "lg:hidden absolute top-0 left-0 right-0 bottom-0 flex flex-col justify-center items-center w-full h-screen bg-green text-center ease-in duration-100"
            : "lg:hidden absolute top-0 left-[-100%] right-0 bottom-0 flex  flex-col justify-center items-center w-full h-screen bg-green text-center ease-in duration-100"
        }
      >
        <div className="flex flex-col space-y-8">
          {navigation?.map((link) => (
            <ul
              key={link?.href}
              className="text-[1rem] mt-[5rem] flex flex-col"
            >
              <li
                className={`text-white text-center uppercase mt-6 ${
                  activeLink === link.href ? "font-semibold" : "font-semibold"
                } hover:font-bold`}
              >
                <Link
                  href={link.href || ""}
                  onClick={() => {
                    setActiveLink(link.href);
                    handleLinkClick(); // Call handleLinkClick to close the mobile menu
                  }}
                >
                  {link.label}
                </Link>
              </li>
            </ul>
          ))}
        </div>

        <div className=" flex flex-col space-y-5 mt-12">
          <div className="flex flex-col space-y-8">
            <button className=" bg-white text-green uppercase py-2  px-11 rounded-none text-center text-base font-bold">
              <Link href={buttons?.Login?.href || ""} target="_blank">
                {buttons?.Login?.label}
              </Link>
            </button>

            <button className="transparent  uppercase  border-2 text-white border-white font-bold py-2 px-16 rounded-none text-center text-base">
              <Link href={buttons?.Enroll?.href || ""} target="_blank">
                {buttons?.Enroll?.label}
              </Link>
            </button>
          </div>

          <Localization data={locales} />
        </div>
      </div>
    </div>
  );
};

export default NavBar;
