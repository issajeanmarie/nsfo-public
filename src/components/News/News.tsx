import Image from "next/image";
import React from "react";
import { ArticleProps } from "../../../lib/types";
import Link from "next/link";
import moment from "moment";

const News: React.FC<ArticleProps> = ({
  title,
  date,
  label,
  slug,
  description,
  image,
  code,
}) => {
  const formattedDate = moment(date).format("ll");

  return (
    <div className="md:space-y-3 space-y-2 flex flex-col justify-start  w-full">
      <div className=" w-full bg-white md:block md:w-full">
        <Link
          // href={`${nav?.href}?lang=${code}`}
          href={`/articles/${slug}?lang=${code}`}
        >
          <Image
            src={image}
            alt=""
            className="w-full   max-h-56 object-center object-cover"
            width={355}
            height={204}
          />
        </Link>
      </div>

      <div className="flex flex-col space-y-2 mt-6  w-full">
        <p className="text-xs italic text-[#919191]">{formattedDate}</p>
        <p className="text-[#656565] text-xs leading-4 font-black  uppercase">
          {title}
        </p>
        <p className=" hidden md:block md:text-xs text-gray">{description}</p>

        <Link
          href={`/articles/${slug}?lang=${code}`}
          className=" text-gray font-semibold text-[0.625em] text-left  capitalize md:w-3/4 w-full mt-6 flex flex-row  items-center"
        >
          {label}
          <Image
            src="/images/arrow.svg"
            alt=""
            className="ml-2"
            width={12}
            height={12}
          />
        </Link>
      </div>
    </div>
  );
};

export default News;
