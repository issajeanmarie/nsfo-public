import _ from "lodash";

type PaginationProps = {
  items: number;
  pageSize: number;
  currentPage: number;
  onPageChange: any;
};

const Pagination = ({
  items,
  pageSize,
  currentPage,
  onPageChange,
}: PaginationProps) => {
  const pageCount = Math.ceil(items / pageSize);
  if (pageCount === 1) return null;
  const pages = _.range(1, pageCount + 1);

  const handleNextPage = () => {
    if (currentPage < pageCount) {
      onPageChange(currentPage + 1);
    }
  };

  const handlePreviousPage = () => {
    if (currentPage > 1) {
      onPageChange(currentPage - 1);
    }
  };

  return (
    <div className="flex flex-row bg-white w-full justify-between items-center px-5 mt-2">
      {currentPage > 1 ? (
        <button
          onClick={handlePreviousPage}
          className="text-green text-xs text-right"
        >
          Previous
        </button>
      ) : (
        <div></div>
      )}
      <nav className="py-3">
        <ul className=" flex flex-row items-center justify-center space-x-2 ">
          {pages.map((page) => (
            <li
              key={page}
              className={
                page === currentPage
                  ? "bg-green py-1 px-3   border-green text-white"
                  : "bg-white py-1 px-3 border-2  border-[#DBDBDB] text-gray"
              }
            >
              <a
                style={{ cursor: "pointer" }}
                onClick={() => onPageChange(page)}
                className="page-link"
              >
                {page}
              </a>
            </li>
          ))}
        </ul>
      </nav>
      {currentPage < pageCount ? (
        <button
          onClick={handleNextPage}
          className="text-green text-xs text-right"
        >
          Next
        </button>
      ) : (
        <div></div>
      )}
    </div>
  );
};

export default Pagination;
