import React from "react";

import { ServicetProps } from "../../../lib/types";
const Services: React.FC<ServicetProps> = ({ number, title, content }) => {
  return (
    <div className="">
      <div className="space-y-3 w-[250px] md:w-full  lg:px-12 ">
        <p className=" text-4xl font-light  text-green">{number}</p>
        <p className="text-[14px] leading-7 md:text-lg font-black text-black">
          {title}
        </p>
        <p className="text-gray text-xs leading-5 md:text-sm md:leading-6 font-normal">
          {content}
        </p>
      </div>
    </div>
  );
};
export default Services;
