import Image from "next/image";
import React from "react";
import { TermsProps } from "../../../lib/types";

function Terms(terms: TermsProps) {
  return (
    <div className="flex flex-col space-y-3 md:space-y-6 py-6 border-b-2 border-[#459D684D] w-full md:pr-12 lg:pr-24">
      <Image
        src={terms.image}
        alt=""
        className="h-8 w-8 "
        width={40}
        height={40}
      />
      <p className="text-xs text-[#00461C] leading-5 font-semibold md:text-sm">
        {terms.title}
      </p>
      <p className="text-gray text-xs leading-5">{terms.content}</p>
    </div>
  );
}

export default Terms;
