"use client";
import {
  Data4,
  Attributes9,
  Navigation,
} from "../../../lib/types/AboutPageTypes";
import React, { useState } from "react";
import Image from "next/image";
import Button from "../atoms/Button";

interface SwitchProps {
  content: {
    data: Data4;
    link: Navigation;
  };
}

export const Switch = ({ content }: SwitchProps) => {
  const switchContent = content?.data?.attributes?.categories?.data;

  const [selectedItem, setSelectedItem] = useState<Attributes9 | null>(
    switchContent && switchContent.length > 0
      ? switchContent[0]?.attributes
      : null
  );

  const handleItemClick = (index: number) => {
    switchContent && setSelectedItem(switchContent[index]?.attributes || null);
  };

  return (
    <div className="flex flex-col">
      <div className="flex flex-row items-start justify-start md:space-x-12 lg:space-x-24 md:h-[40vh] lg:h-[60vh] xxl:h-[40vh]">
        <div className="flex flex-col space-y-6 w-2/3 md:w-2/8 lg:w-2/5  mt-24 md:mt-12 lg:mt-24 ">
          {switchContent?.map((item, index) => (
            <li
              className={`text-[#656565] text-xs uppercase list-none cursor-pointer ${
                selectedItem?.title === item.attributes.title
                  ? "font-bold"
                  : "font-normal"
              }`}
              key={item.id}
              onClick={() => handleItemClick(index)}
            >
              {item?.attributes?.title}
            </li>
          ))}
        </div>

        {selectedItem && (
          <div className=" flex justify-start items-start relative w-full md:w-2/8 lg:w-3/5  md:py-4 lg:-mt-12 md:mt-0 mt-12 h-[40vh] md:h-full">
            <Image
              src={selectedItem?.image?.data?.attributes?.url}
              width={440}
              height={519}
              className=" object-cover object-center z-[5] w-full h-full "
              alt=""
            />

            <div className=" bg-green absolute lg:w-1/2 lg:h-1/2 h-1/2 w-1/2 md:w-1/2 md:h-1/2 lg:-right-8 lg:-top-0 md:-right-4 md:-top-4 -right-2 -top-2 z-0"></div>
            <div className="bg-green absolute lg:w-1/2 lg:h-1/2 h-1/2 w-1/2 md:w-36 md:h-40 lg:-left-8  md:-left-4 -left-2   md:-bottom-0 lg:-bottom-0 -bottom-2 z-0"></div>
          </div>
        )}

        {selectedItem && (
          <div className="space-y-12 lg:relative bottom-0 left-0 w-full hidden lg:flex flex-col lg:w-3/8 lg:mt-24">
            <p className="text-gray  text-[14px] leading-7  ">
              {selectedItem?.description}
            </p>

            <Button label={content.link.label} link={content.link.href} />
          </div>
        )}
      </div>

      {selectedItem && (
        <div className="lg:hidden flex flex-col space-y-6 relative mt-6 bottom-0 left-0 ">
          <p className="text-gray  text-[14px] leading-7">
            {selectedItem?.description}
          </p>
          <Button label={content.link.label} link={content.link.href} />
        </div>
      )}
    </div>
  );
};
