import { Suspense } from "react";

const APIFallback = ({ children }: { children: React.ReactNode }) => {
	return (
		// nprogress spinner 
		<Suspense
			fallback={
				<div className="w-full h-screen flex items-center justify-center">
					<span className="text-[14px] text-black">Loading....</span>
				</div>
			}
		>
			{children}
		</Suspense>
	);
};

export default APIFallback;
