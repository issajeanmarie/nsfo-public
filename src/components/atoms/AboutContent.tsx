import Image from "next/image";
import React from "react";
import HeaderText from "../HeaderText/HeaderText";
import Button from "./Button";

type ContentProps = {
  text: string;
  header: string;
  image: string;
  label: string;
  link: string;
  content: string;
};
const AboutContent: React.FC<ContentProps> = ({
  label,
  header,
  text,
  image,
  link,
  content,
}) => {
  return (
    <div className="flex flex-col  justify-between items-end w-full h-full md:h-2/3 p-5  md:px-16 lg:px-[6rem]  lg:py-[3rem] xl:px-[12rem] py-6 md:py-16 xxl:max-w-[2100px]  xxl:mx-auto  ">
      <div className="grid grid-cols-1 md:grid-cols-2 gap-6">
        <div className=" flex flex-col items-start justify-between ">
          <HeaderText title={label} header={header} text={""} />
          <p className="mb-6 md:mb-12 text-gray text-[14px] leading-7 lg:pr-24">
            {text}
          </p>
          <div className="md:w-4/6 w-full">
            <Button link={link} label={content} />
          </div>
        </div>

        <div className=" hidden md:flex  md:mt-16 items-end  justify-end">
          <Image
            src={image || ""}
            alt=""
            width={500}
            height={800}
            className="object-cover  lg:w-5/6 float-right"
          />
        </div>
      </div>
    </div>
  );
};

export default AboutContent;
