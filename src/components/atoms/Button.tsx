import Image from "next/image";
import Link from "next/link";
import React from "react";

type ButtonProps = {
  label: string;
  link: string;
};

const Button: React.FC<ButtonProps> = ({ label, link }) => {
  return (
    <button className="flex flex-row items-center justify-center w-full lg:w-2/4  space-x-6  bg-gradient-to-r from-green to-forest text-white text-[10px] px-[4rem] md:px-4 py-3 font-bold text-center uppercase md:underline ">
      <Image
        src="/images/down.png"
        alt=""
        className="object-cover"
        width={19}
        height={24}
      />
      <Link target="_blank" href={link || ""}>
        {label}
      </Link>
    </button>
  );
};

export default Button;
