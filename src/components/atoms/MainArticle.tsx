import React from "react";
import Image from "next/image";
import { MainArticleProp } from "../../../lib/types";
import Link from "next/link";

const MainArticle: React.FC<MainArticleProp> = ({
  title,
  description,
  slug,
  label,
  image,
  code,
}) => {
  return (
    <div className="grid grid-cols-1 lg:grid-cols-2 gap-x-16 gap-y-6  bg-green]">
      <div className=" flex xl:h-[40vh] ">
        <Image
          src={image}
          alt=""
          width={830}
          height={514}
          className="w-full  object-cover  object-center "
        />
      </div>
      <div className="flex flex-col justify-between items-start w-full xl:h-[40vh] ">
        <p className="font-extrabold text-lg leading-7  text-gray   md:text-3xl  md:leading-loose lg:text-4xl lg:leading-[3rem] text-left uppercase lg:pr-32">
          {title}
        </p>
        <p className="text-gray text-sm leading-5 lg:pr-16 font-normal w-full md:text-base md:leading-8 mt-4 lg:mt-8">
          {description}
        </p>
        <button className=" text-gray font-semibold text-[14px] lg:text-[1rem] text-left capitalize md:w-3/4 w-full mt-6 flex flex-row items-center ">
          <Link
            // href={`/articles/${slug || ""}`}
            href={`/articles/${slug}?lang=${code}`}
            className=" text-gray font-semibold text-sm text-left  capitalize md:w-3/4 w-full mt-6 flex flex-row  items-center underline"
          >
            {label}
            <Image
              src="/images/arrow.svg"
              alt=""
              className="ml-2 "
              width={24}
              height={8}
            />
          </Link>
        </button>
      </div>
    </div>
  );
};

export default MainArticle;
