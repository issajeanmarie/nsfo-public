import React from "react";
import Image from "next/image";
import { ArticleProps } from "../../../lib/types";
import Link from "next/link";

const MainNews: React.FC<ArticleProps> = ({
  title,
  slug,
  label,
  link,
  description,
  image,
  code
}) => {
  return (
    <>
      <p className="md:hidden font-extrabold text-[1.5rem] text-green mb-8 ">
        {label}
      </p>

      <div className="w-full flex flex-col-reverse md:flex-row lg:h-[75vh] xxl:h-[60vh]">
        <div className="w-full md:h-[40vh] lg:h-full lg:w-1/2 justify-between items-start flex flex-col ">
          <div className="flex flex-col  md:w-full lg:w-4/5 ">
            <p className="font-semibold text-xs text-[#000]  uppercase  hidden lg:block ">
              {label}
            </p>
            <div className="flex flex-col py-4 space-y-3 md:space-y-8 md:pr-16 lg:pr-0">
              <div className="flex flex-row lg:space-x-4 ">
                <div className="h-[45px] bg-green  w-1 mt-4 rounded-lg hidden lg:block md:-ml-6"></div>
                <div className="w-full">
                  <p className="font-extrabold text-lg leading-7 md:text-[1.5rem]  lg:text-[2.5rem] lg:leading-[3.5rem]  text-gray text-left md:text-green uppercase">
                    {title}
                  </p>
                </div>
              </div>
              <p className="text-gray text-xs leading-5 font-normal w-full  lg:text-lg  lg:mb-4">
                {description}
              </p>
            </div>
          </div>

          <div className="lg:w-1/2 w-full ">
            <button className="bg-gradient-to-r from-green to-forest text-white font-semibold px-12 md:px-4 py-3 text-xs md:text-sm rounded-none text-center uppercase lg:w-4/6 md:w-1/2 w-full">
              <Link
                href={`/articles/${slug}?lang=${code}`}
                // href={`articles/${slug || ""}`}
              
              >{link}</Link>
            </button>
          </div>
        </div>

        <div className="w-full lg:w-1/2 lg:mt-6">
          <Image
            src={image}
            alt=""
            width={830}
            height={514}
            className="w-full h-[30vh] md:h-[40vh] lg:h-full object-center object-cover float-right"
          />
        </div>
      </div>
    </>
  );
};

export default MainNews;
