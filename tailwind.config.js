/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      fontFamily: {
        lato: ["Lato", "sans-serif"],
      },
    },
    screens: {
      sm: "680px",
      md: "768px",
      lg: "1280px",
      xl: "1920px",
      xxl: "2000px",
    },
    colors: {
      // primary colors
      white: "#fff",
      black: "#000",
      gray: "#656565",
      //secondary
      forest: "#347E52",
      green: "#459D68",
      //accent
      blue: "rgb(22 78 99)",
      yellow: "rgb(252 211 77)",
      cyan: "#91919126",
    },
    plugins: [],
  },
};
