interface Props {
  endpoint: string;
  language?: string | null;
  filters?: string | null;
  sort?: string | null;
}

export const handleAPIRequests = async ({
  endpoint,
  language = "nl",
  filters = "",
  sort = "",
}: Props) => {
  let apiUrl = `https://api-nsfo.awesomity.rw/api/${endpoint}?populate=deep&locale=${
    language || "en"
  }${sort ? "&sort=" + sort : ""}${filters ? "&" + filters : ""}
 `;
  const response = await fetch(apiUrl, {
    cache: "force-cache",
  });
  return response.json();
};
